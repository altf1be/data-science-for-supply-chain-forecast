import { useEffect, useState } from "react";
import jspreadsheet from "jspreadsheet";
import "jspreadsheet/dist/jspreadsheet.css";
import "jsuites/dist/jsuites.css";
import parser from "@jspreadsheet/parser";
import Stepper from "../../components/Stepper";
import {Router, useRouter} from 'next/router';

jspreadsheet.setLicense(
  "ZjI5NWE5YmI0YzZiMjZmODRkMTg2Y2RlZDNlZjQzYWUxMTk0N2Q3NzRkYmRhZTBhOTQyNGQ3YmUwZjVlZjUzZTQzZjJmNzE3MjliNTZiMzQwZjU0YTczMWI4ZjQ3MWYzZmI3MDc5YTBmZTc3NzhjY2I0ZmZiOWVmZWYwMDRlMzksZXlKdVlXMWxJam9pUkVKZlpYaHdaWEp0YVc1MGFXNW5JaXdpWkdGMFpTSTZNVFkzTVRBMk1qUXdNQ3dpWkc5dFlXbHVJanBiSWlJc0lteHZZMkZzYUc5emRDSmRMQ0p3YkdGdUlqb3dMQ0p6WTI5d1pTSTZXeUoyTnlJc0luWTRJaXdpZGpraVhYMD0="
);
jspreadsheet.setExtensions({ parser });

function Forecast() {
  const [sheet, setSheet] = useState<any>();
  const [forecast, setForecast] = useState<any>(null);
  const router = useRouter()
  const crumps= router.asPath.split('/')
  const updatePreview = async (current_sheet: any) => {
    jspreadsheet.parser({
      file: current_sheet,
      onload: function (config) {
        jspreadsheet(document.getElementById("source_spreadsheet"), config);
      },
    });
  };

  const updateForm = async (current_sheet: any) => {
    jspreadsheet.parser({
      file: current_sheet,
      onload: function (config) {
        jspreadsheet(document.getElementById("forecast_spreadsheet"), config);
      },
    });
  };

  useEffect(() => {
    updatePreview(sheet);
  }, [sheet]);
  useEffect(() => {}, [sheet, forecast]);

  return (
    <>
      <div className="row wrapper white-bg">
        <div className="col-md-12 text-center">
          <h3> Demand Forecast</h3>
          {/* <ol className="breadcrumb">
                        <li className="breadcrumb-item">
                            <a href="">Home</a>
                        </li>
                        <li className="breadcrumb-item">
                            <a>{crumps[1]}</a>
                        </li>
                        <li className="breadcrumb-item active">
                            <strong>{crumps[2]}</strong>
                        </li>
                    </ol> */}
        </div>
      </div>
      <div className="wrapper wrapper-content">
        <Stepper></Stepper>
      </div>
    </>
  );
}

export default Forecast;
