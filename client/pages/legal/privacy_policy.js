import Head from "next/head";
import { htmlMetaTags } from "../../fixtures/html.head.meta.js";

export async function getServerSideProps(context) {
  return {
    props: {
      htmlMetaTagsList: htmlMetaTags,
    },
  };
}
export default function PrivacyPolicyPage({ htmlMetaTagsList }) {
  return (
    <>
     <Head>
        <meta charSet="utf-8" />
        <link rel="manifest" href="/manifest.json" />
        <title>Privacy Policy - Facility Management Platform - FM Tech</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        {htmlMetaTagsList.map((htmlMetaTag) => (
          <meta
            key={htmlMetaTag.meta}
            name={htmlMetaTag.meta}
            content={htmlMetaTag.content}
          />
        ))}
      </Head>
      <p>
        <strong>PRIVACY NOTICE</strong>
      </p>
      <p>
        <strong>Last updated September 07, 2020</strong>
      </p>
      <p>
        Thank you for choosing to be part of our community at ALT-F1 SPRL, doing
        business as FM Tech (“ <strong>FM Tech</strong> ”, “<strong>we</strong>
        ”, “<strong>us</strong>”, or “ <strong>our</strong>”). We are committed
        to protecting your personal information and your right to privacy. If
        you have any questions or concerns about this privacy notice, or our
        practices with regards to your personal information, please contact us
        at abo+fm_tech_privacy_policy@alt-f1.be.
      </p>
      <p>
        When you visit our website{" "}
        <a href="https://fmplatform.azurewebsites.net">
          https://fmplatform.azurewebsites.net
        </a>{" "}
        (the "<strong>Website</strong>"), use our mobile application, as the
        case may be (the "<strong>App</strong>") and more generally, use any of
        our services (the "<strong>Services</strong>", which include the Website
        and App ), we appreciate that you are trusting us with your personal
        information. We take your privacy very seriously. In this privacy
        notice, we seek to explain to you in the clearest way possible what
        information we collect, how we use it and what rights you have in
        relation to it. We hope you take some time to read through it carefully,
        as it is important. If there are any terms in this privacy notice that
        you do not agree with, please discontinue use of our Services
        immediately.
      </p>
      <p>
        This privacy notice applies to all information collected through our
        Services (which, as described above, includes our Website and App ), as
        well as any related services, sales, marketing or events.
      </p>
      <p>
        <strong>
          Please read this privacy notice carefully as it will help you
          understand what we do with the information that we collect.
        </strong>
      </p>
      <p>
        <strong>TABLE OF CONTENTS</strong>
      </p>
      <p>
        <a href="#infocollect">1. WHAT INFORMATION DO WE COLLECT?</a>
      </p>
      <p>
        <a href="#infouse">2. HOW DO WE USE YOUR INFORMATION?</a>
      </p>
      <p>
        <a href="#infoshare">3. WILL YOUR INFORMATION BE SHARED WITH ANYONE?</a>
      </p>
      <p>
        <a href="#whoshare">4. WHO WILL YOUR INFORMATION BE SHARED WITH?</a>
      </p>
      <p>
        <a href="#cookies">
          5. DO WE USE COOKIES AND OTHER TRACKING TECHNOLOGIES?
        </a>
      </p>
      <p>
        <a href="#sociallogins">6. HOW DO WE HANDLE YOUR SOCIAL LOGINS?</a>
      </p>
      <p>
        <a href="#intltransfers">
          7. IS YOUR INFORMATION TRANSFERRED INTERNATIONALLY?
        </a>
      </p>
      <p>
        <a href="#inforetain">8. HOW LONG DO WE KEEP YOUR INFORMATION?</a>
      </p>
      <p>
        <a href="#infosafe">9. HOW DO WE KEEP YOUR INFORMATION SAFE?</a>
      </p>
      <p>
        <a href="#infominors">10. DO WE COLLECT INFORMATION FROM MINORS?</a>
      </p>
      <p>
        <a href="#privacyrights">11. WHAT ARE YOUR PRIVACY RIGHTS?</a>
      </p>
      <p>
        <a href="#DNT">12. CONTROLS FOR DO-NOT-TRACK FEATURES</a>
      </p>
      <p>
        <a href="#caresidents">
          13. DO CALIFORNIA RESIDENTS HAVE SPECIFIC PRIVACY RIGHTS?
        </a>
      </p>
      <p>
        <a href="#policyupdates">14. DO WE MAKE UPDATES TO THIS NOTICE?</a>
      </p>
      <p>
        <a href="#contact">15. HOW CAN YOU CONTACT US ABOUT THIS NOTICE?</a>
      </p>
      <p>
        <strong>1. WHAT INFORMATION DO WE COLLECT?</strong>
      </p>
      <p>
        <strong>
          <strong>Personal information you disclose to us</strong>
        </strong>
      </p>
      <p>
        <strong>In Short:</strong> We collect information that you provide to
        us.
      </p>
      <p>
        We collect personal information that you voluntarily provide to us when
        you register on the Services, express an interest in obtaining
        information about us or our products and Services, when you participate
        in activities on the Services (such as by posting messages in our online
        forums or entering competitions, contests or giveaways) or otherwise
        when you contact us.
      </p>
      <p>
        The personal information that we collect depends on the context of your
        interactions with us and the Services, the choices you make and the
        products and features you use. The personal information we collect may
        include the following:
      </p>
      <p>
        <strong>Personal Information Provided by You.</strong> We collect{" "}
        names;email addresses; mailing addresses;usernames; passwords;billing
        addresses; debit/credit card numbers; and other similar information.
      </p>
      <p>
        <strong>Payment Data.</strong> We may collect data necessary to process
        your payment if you make purchases, such as your payment instrument
        number (such as a credit card number), and the security code associated
        with your payment instrument. All payment data is stored by Twikey ,{" "}
        Stripe and Mollie . You may find their privacy notice link(s) here:{" "}
        <a href="https://www.twikey.com/nl/privacy.html">
          https://www.twikey.com/nl/privacy.html
        </a>{" "}
        ,{" "}
        <a href="https://stripe.com/en-be/privacy">
          https://stripe.com/en-be/privacy
        </a>{" "}
        and{" "}
        <a href="https://www.mollie.com/en/privacy">
          https://www.mollie.com/en/privacy
        </a>{" "}
        .
      </p>
      <p>
        <strong>Social Media Login Data.</strong> We may provide you with the
        option to register with us using your existing social media account
        details, like your Facebook, Twitter or other social media account. If
        you choose to register in this way, we will collect the information
        described in the section called "{" "}
        <a href="#sociallogins">HOW DO WE HANDLE YOUR SOCIAL LOGINS</a>" below.
      </p>
      <p>
        All personal information that you provide to us must be true, complete
        and accurate, and you must notify us of any changes to such personal
        information.
      </p>
      <p>
        <strong>
          <strong>Information automatically collected</strong>
        </strong>
      </p>
      <p>
        <strong>In Short:</strong> Some information — such as your Internet
        Protocol (IP) address and/or browser and device characteristics — is
        collected automatically when you visit our Services .
      </p>
      <p>
        We automatically collect certain information when you visit, use or
        navigate the Services . This information does not reveal your specific
        identity (like your name or contact information) but may include device
        and usage information, such as your IP address, browser and device
        characteristics, operating system, language preferences, referring URLs,
        device name, country, location, information about who and when you use
        our Services and other technical information. This information is
        primarily needed to maintain the security and operation of our Services
        , and for our internal analytics and reporting purposes.
      </p>
      <p>
        Like many businesses, we also collect information through cookies and
        similar technologies.{" "}
      </p>
      <p>The information we collect includes:</p>
      <ul>
        <li>
          Log and Usage Data. Log and usage data is service-related, diagnostic
          usage and performance information our servers automatically collect
          when you access or use our Services and which we record in log files.
          Depending on how you interact with us, this log data may include your
          IP address, device information, browser type and settings and
          information about your activity in the Services (such as the date/time
          stamps associated with your usage, pages and files viewed, searches
          and other actions you take such as which features you use), device
          event information (such as system activity, error reports (sometimes
          called 'crash dumps') and hardware settings).
        </li>
      </ul>
      <ul>
        <li>
          Device Data. We collect device data such as information about your
          computer, phone, tablet or other device you use to access the Services
          . Depending on the device used, this device data may include
          information such as your IP address (or proxy server), device
          application identification numbers, location, browser type, hardware
          model Internet service provider and/or mobile carrier, operating
          system configuration information.
        </li>
      </ul>
      <ul>
        <li>
          Location Data. We collect information data such as information about
          your device's location, which can be either precise or imprecise. How
          much information we collect depends on the type of settings of the
          device you use to access the Services . For example, we may use GPS
          and other technologies to collect geolocation data that tells us your
          current location (based on your IP address). You can opt out of
          allowing us to collect this information either by refusing access to
          the information or by disabling your Locations settings on your
          device. Note however, if you choose to opt out, you may not be able to
          use certain aspects of the Services.
        </li>
      </ul>
      <p>
        <strong>
          <strong>Information collected through our App</strong>
        </strong>
      </p>
      <p>
        <strong>In Short:</strong> We collect information regarding your
        geo-location, mobile device, push notifications, when you use our App.
      </p>
      <p>If you use our App, we also collect the following information:</p>
      <ul>
        <li>
          Geo-Location Information. We may request access or permission to and
          track location-based information from your mobile device, either
          continuously or while you are using our App, to provide certain
          location-based services. If you wish to change our access or
          permissions, you may do so in your device's settings.
        </li>
      </ul>
      <ul>
        <li>
          Mobile Device Access. We may request access or permission to certain
          features from your mobile device, including your mobile device's
          sensors, and other features. If you wish to change our access or
          permissions, you may do so in your device's settings.
        </li>
      </ul>
      <ul>
        <li>
          Mobile Device Data. We automatically collect device information (such
          as your mobile device ID, model and manufacturer), operating system,
          version information and system configuration information, device and
          application identification numbers, browser type and version, hardware
          model Internet service provider and/or mobile carrier, and Internet
          Protocol (IP) address (or proxy server). If you are using our App, we
          may also collect information about the phone network associated with
          your mobile device, your mobile device's operating system or platform,
          the type of mobile device you use, your mobile device's unique device
          ID and information about the features of our App you accessed.
        </li>
      </ul>
      <ul>
        <li>
          Push Notifications. We may request to send you push notifications
          regarding your account or certain features of the App. If you wish to
          opt-out from receiving these types of communications, you may turn
          them off in your device's settings.
        </li>
      </ul>
      <p>
        The information is primarily needed to maintain the security and
        operation of our App, for troubleshooting and for our internal analytics
        and reporting purposes.
      </p>
      <p>
        <strong>
          <strong>Information collected from other sources</strong>
        </strong>
      </p>
      <p>
        <strong>In Short: </strong> We may collect limited data from public
        databases, marketing partners, social media platforms, and other outside
        sources.
      </p>
      <p>
        In order to enhance our ability to provide relevant marketing, offers
        and services to you and update our records, we may obtain information
        about you from other sources, such as public databases, joint marketing
        partners, affiliate programs, data providers, social media platforms, as
        well as from other third parties. This information includes mailing
        addresses, job titles, email addresses, phone numbers, intent data (or
        user behavior data), Internet Protocol (IP) addresses, social media
        profiles, social media URLs and custom profiles, for purposes of
        targeted advertising and event promotion. If you interact with us on a
        social media platform using your social media account (e.g. Facebook or
        Twitter), we receive personal information about you such as your name,
        email address, and gender. Any personal information that we collect from
        your social media account depends on your social media account's privacy
        settings.
      </p>
      <p>
        <strong>2. HOW DO WE USE YOUR INFORMATION?</strong>
      </p>
      <p>
        <strong>In Short:</strong> We process your information for purposes
        based on legitimate business interests, the fulfillment of our contract
        with you, compliance with our legal obligations, and/or your consent.
      </p>
      <p>
        We use personal information collected via our Services for a variety of
        business purposes described below. We process your personal information
        for these purposes in reliance on our legitimate business interests, in
        order to enter into or perform a contract with you, with your consent,
        and/or for compliance with our legal obligations. We indicate the
        specific processing grounds we rely on next to each purpose listed
        below.
      </p>
      <p>We use the information we collect or receive:</p>
      <ul>
        <li>
          <strong>To facilitate account creation and logon process.</strong> If
          you choose to link your account with us to a third-party account (such
          as your Google or Facebook account), we use the information you
          allowed us to collect from those third parties to facilitate account
          creation and logon process for the performance of the contract. See
          the section below headed "{" "}
          <a href="#sociallogins">HOW DO WE HANDLE YOUR SOCIAL LOGINS</a> " for
          further information.
        </li>
        <li>
          <strong>To post testimonials.</strong> We post testimonials on our{" "}
          Services that may contain personal information. Prior to posting a
          testimonial, we will obtain your consent to use your name and the
          consent of the testimonial. If you wish to update, or delete your
          testimonial, please contact us at abo+fm_platform_dpo@alt-f1.be and be
          sure to include your name, testimonial location, and contact
          information.
        </li>
        <li>
          <strong>Request feedback.</strong> We may use your information to
          request feedback and to contact you about your use of our Services .
        </li>
        <li>
          <strong>To enable user-to-user communications.</strong> We may use
          your information in order to enable user-to-user communications with
          each user's consent.
        </li>
        <li>
          <strong>To manage user accounts</strong>. We may use your information
          for the purposes of managing our account and keeping it in working
          order.
        </li>
      </ul>
      <ul>
        <li>
          <strong>To send administrative information to you.</strong> We may use
          your personal information to send you product, service and new feature
          information and/or information about changes to our terms, conditions,
          and policies.
        </li>
        <li>
          <strong>To protect our Services.</strong> We may use your information
          as part of our efforts to keep our Services safe and secure (for
          example, for fraud monitoring and prevention).
        </li>
        <li>
          <strong>
            To enforce our terms, conditions and policies for business purposes,
            to comply with legal and regulatory requirements or in connection
            with our contract.
          </strong>
        </li>
        <li>
          <strong>To respond to legal requests and prevent harm.</strong> If we
          receive a subpoena or other legal request, we may need to inspect the
          data we hold to determine how to respond.
        </li>
      </ul>
      <ul>
        <li>
          <strong>Fulfill and manage your orders.</strong> We may use your
          information to fulfill and manage your orders, payments, returns, and
          exchanges made through the Services .
        </li>
        <li>
          <strong>Administer prize draws and competitions.</strong> We may use
          your information to administer prize draws and competitions when you
          elect to participate in our competitions.
        </li>
        <li>
          <strong>
            To deliver and facilitate delivery of services to the user.
          </strong>{" "}
          We may use your information to provide you with the requested service.
        </li>
        <li>
          <strong>To respond to user inquiries/offer support to users.</strong>{" "}
          We may use your information to respond to your inquiries and solve any
          potential issues you might have with the use of our Services.
        </li>
      </ul>
      <ul>
        <li>
          <strong>To send you marketing and promotional communications.</strong>{" "}
          We and/or our third-party marketing partners may use the personal
          information you send to us for our marketing purposes, if this is in
          accordance with your marketing preferences. For example, when
          expressing an interest in obtaining information about us or our{" "}
          Services , subscribing to marketing or otherwise contacting us, we
          will collect personal information from you. You can opt-out of our
          marketing emails at any time (see the "{" "}
          <a href="#privacyrights">WHAT ARE YOUR PRIVACY RIGHTS</a> " below).
        </li>
        <li>
          <strong>Deliver targeted advertising to you.</strong> We may use your
          information to develop and display personalized content and
          advertising (and work with third parties who do so) tailored to your
          interests and/or location and to measure its effectiveness.
        </li>
      </ul>
      <ul>
        <li>
          <strong>For other business purposes.</strong> We may use your
          information for other business purposes, such as data analysis,
          identifying usage trends, determining the effectiveness of our
          promotional campaigns and to evaluate and improve our Services ,
          products, marketing and your experience. We may use and store this
          information in aggregated and anonymized form so that it is not
          associated with individual end users and does not include personal
          information. We will not use identifiable personal information without
          your consent.
        </li>
      </ul>
      <p>
        <strong>3. WILL YOUR INFORMATION BE SHARED WITH ANYONE?</strong>
      </p>
      <p>
        <strong>In Short: </strong> We only share information with your consent,
        to comply with laws, to provide you with services, to protect your
        rights, or to fulfill business obligations.
      </p>
      <p>
        We may process or share your data that we hold based on the following
        legal basis:
      </p>
      <ul>
        <li>
          <strong>Consent:</strong> We may process your data if you have given
          us specific consent to use your personal information in a specific
          purpose.
        </li>
        <li>
          <strong>Legitimate Interests:</strong> We may process your data when
          it is reasonably necessary to achieve our legitimate business
          interests.
        </li>
        <li>
          <strong>Performance of a Contract:</strong> Where we have entered into
          a contract with you, we may process your personal information to
          fulfill the terms of our contract.
        </li>
        <li>
          <strong>Legal Obligations:</strong> We may disclose your information
          where we are legally required to do so in order to comply with
          applicable law, governmental requests, a judicial proceeding, court
          order, or legal process, such as in response to a court order or a
          subpoena (including in response to public authorities to meet national
          security or law enforcement requirements).
        </li>
        <li>
          <strong>Vital Interests:</strong> We may disclose your information
          where we believe it is necessary to investigate, prevent, or take
          action regarding potential violations of our policies, suspected
          fraud, situations involving potential threats to the safety of any
          person and illegal activities, or as evidence in litigation in which
          we are involved.
        </li>
      </ul>
      <p>
        More specifically, we may need to process your data or share your
        personal information in the following situations:
      </p>
      <ul>
        <li>
          <strong>Business Transfers.</strong> We may share or transfer your
          information in connection with, or during negotiations of, any merger,
          sale of company assets, financing, or acquisition of all or a portion
          of our business to another company.
        </li>
      </ul>
      <ul>
        <li>
          <strong>
            Vendors, Consultants and Other Third-Party Service Providers.
          </strong>{" "}
          We may share your data with third-party vendors, service providers,
          contractors or agents who perform services for us or on our behalf and
          require access to such information to do that work. Examples include:
          payment processing, data analysis, email delivery, hosting services,
          customer service and marketing efforts. We may allow selected third
          parties to use tracking technology on the Services , which will enable
          them to collect data on our behalf about how you interact with our{" "}
          Services over time. This information may be used to, among other
          things, analyze and track data, determine the popularity of certain
          content, pages or features, and better understand online activity.
          Unless described in this notice, we do not share, sell, rent or trade
          any of your information with third parties for their promotional
          purposes. We have contracts in place with our data processors, which
          are designed to help safegaurd your personal information. This means
          that they cannot do anything with your personal information unless we
          have instructed them to do it. They will also not share your personal
          information with any organization apart from us. They also commit to
          protect the data they hold on our behalf and to retain it for the
          period we instruct.
        </li>
      </ul>
      <ul>
        <li>
          <strong>Third-Party Advertisers.</strong> We may use third-party
          advertising companies to serve ads when you visit or use the Services
          . These companies may use information about your visits to our
          Website(s) and other websites that are contained in web cookies and
          other tracking technologies in order to provide advertisements about
          goods and services of interest to you.{" "}
        </li>
      </ul>
      <ul>
        <li>
          <strong>Affiliates.</strong> We may share your information with our
          affiliates, in which case we will require those affiliates to honor
          this privacy notice. Affiliates include our parent company and any
          subsidiaries, joint venture partners or other companies that we
          control or that are under common control with us.
        </li>
      </ul>
      <ul>
        <li>
          <strong>Business Partners.</strong> We may share your information with
          our business partners to offer you certain products, services or
          promotions.
        </li>
      </ul>
      <ul>
        <li>
          <strong>Other Users.</strong> When you share personal information (for
          example, by posting comments, contributions or other content to the{" "}
          Services ) or otherwise interact with public areas of the Services ,
          such personal information may be viewed by all users and may be
          publicly made available outside the Services in perpetuity. If you
          interact with other users of our Services and register for our{" "}
          Services through a social network (such as Facebook), your contacts on
          the social network will see your name, profile photo, and descriptions
          of your activity. Similarly, other users will be able to view
          descriptions of your activity, communicate with you within our{" "}
          Services , and view your profile.
        </li>
      </ul>
      <ul>
        <li>
          <strong>Offer Wall.</strong> Our App may display a third-party hosted
          “offer wall.” Such an offer wall allows third-party advertisers to
          offer virtual currency, gifts, or other items to users in return for
          the acceptance and completion of an advertisement offer. Such an offer
          wall may appear in our App and be displayed to you based on certain
          data, such as your geographic area or demographic information. When
          you click on an offer wall, you will be brought to an external website
          belonging to other persons and will leave our App. A unique
          identifier, such as your user ID, will be shared with the offer wall
          provider in order to prevent fraud and properly credit your account
          with the relevant reward. Please note that we do not control
          third-party websites and have no responsibility in relation to the
          content of such websites. The inclusion of a link towards a
          third-party website does not imply an endorsement by us of such
          website. Accordingly, we do not make any warranty regarding such
          third-party websites and we will not be liable for any loss or damage
          caused by the use of such websites. In addition, when you access any
          third-party website, please understand that your rights while
          accessing and using those websites will be governed by the privacy
          notice and terms of service relating to the use of those websites.
        </li>
      </ul>
      <p>
        <strong>4. WHO WILL YOUR INFORMATION BE SHARED WITH?</strong>
      </p>
      <p>
        <strong>In Short: </strong> We only share information with the following
        third parties.
      </p>
      <p>
        We only share and disclose your information with the following third
        parties. We have categorized each party so that you may easily
        understand the purpose of our data collection and processing practices.
        If we have processed your data based on your consent and you wish to
        revoke your consent, please contact us using the contact details
        provided in the section below titled "{" "}
        <a href="#contact">HOW CAN YOU CONTACT US ABOUT THIS NOTICE?</a> ".
      </p>
      <ul>
        <li>
          <strong>Advertising, Direct Marketing, and Lead Generation</strong>{" "}
          Google AdSense , AdMob , Bing Ads , HubSpot CRM , Criteo , Intercom{" "}
          and ZOHO CRM
        </li>
      </ul>
      <ul>
        <li>
          <strong>Allow Users to Connect to Their Third-Party Accounts</strong>{" "}
          LinkedIn account , Dropbox account , Facebook account , Stripe account
          , Twitter account , Google account and Github account
        </li>
      </ul>
      <ul>
        <li>
          <strong>Cloud Computing Services</strong> Microsoft Azure and Google
          Cloud Platform
        </li>
      </ul>
      <ul>
        <li>
          <strong>Communicate and Chat with Users</strong> MailChimp{" "}
        </li>
      </ul>
      <ul>
        <li>
          <strong>Content Optimization</strong> Google Fonts and YouTube video
          embed
        </li>
      </ul>
      <ul>
        <li>
          <strong>Functionality and Infrastructure Optimization</strong> Google
          App Engine , Microsoft Azure and Termly.io
        </li>
      </ul>
      <ul>
        <li>
          <strong>Invoice and Billing</strong> Stripe , Twikey and Mollie
        </li>
      </ul>
      <ul>
        <li>
          <strong>Retargeting Platforms</strong> Google Analytics Remarketing
        </li>
      </ul>
      <ul>
        <li>
          <strong>Social Media Sharing and Advertising</strong> LinkedIn social
          plugins
        </li>
      </ul>
      <ul>
        <li>
          <strong>User Account Registration and Authentication</strong> Facebook
          Login , GitHub OAuth , Google OAuth 2.0 , LinkedIn OAuth 2.0 , Stripe
          OAuth and Neato Robotics
        </li>
      </ul>
      <ul>
        <li>
          <strong>User Commenting and Forums</strong> Google Tag Manager
        </li>
      </ul>
      <ul>
        <li>
          <strong>Web and Mobile Analytics</strong> Google Analytics
        </li>
      </ul>
      <ul>
        <li>
          <strong>Website Hosting</strong> Weebly
        </li>
      </ul>
      <ul>
        <li>
          <strong>Website Testing</strong> Google Website Optimizer{" "}
        </li>
      </ul>
      <p>
        <strong>5. DO WE USE COOKIES AND OTHER TRACKING TECHNOLOGIES?</strong>
      </p>
      <p>
        <strong>In Short: </strong> We may use cookies and other tracking
        technologies to collect and store your information.
      </p>
      <p>
        We may use cookies and similar tracking technologies (like web beacons
        and pixels) to access or store information. Specific information about
        how we use such technologies and how you can refuse certain cookies is
        set out in our Cookie Notice .
      </p>
      <p>
        <strong>6. HOW DO WE HANDLE YOUR SOCIAL LOGINS?</strong>
      </p>
      <p>
        <strong>In Short: </strong> If you choose to register or log in to our
        services using a social media account, we may have access to certain
        information about you.
      </p>
      <p>
        Our Services offers you the ability to register and login using your
        third-party social media account details (like your Facebook or Twitter
        logins). Where you choose to do this, we will receive certain profile
        information about you from your social media provider. The profile
        Information we receive may vary depending on the social media provider
        concerned, but will often include your name, email address, friends
        list, profile picture as well as other information you choose to make
        public on such social media platform.{" "}
      </p>
      <p>
        We will use the information we receive only for the purposes that are
        described in this privacy notice or that are otherwise made clear to you
        on the relevant Services . Please note that we do not control, and are
        not responsible for, other uses of your personal information by your
        third-party social media provider. We recommend that you review their
        privacy notice to understand how they collect, use and share your
        personal information, and how you can set your privacy preferences on
        their sites and apps.
      </p>
      <p>
        <strong>7. IS YOUR INFORMATION TRANSFERRED INTERNATIONALLY?</strong>
      </p>
      <p>
        <strong>In Short: </strong> We may transfer, store, and process your
        information in countries other than your own.
      </p>
      <p>
        Our servers are located in Ireland . If you are accessing our Services{" "}
        from outside Ireland , please be aware that your information may be
        transferred to, stored, and processed by us in our facilities and by
        those third parties with whom we may share your personal information
        (see "{" "}
        <a href="#infoshare">WILL YOUR INFORMATION BE SHARED WITH ANYONE?</a> "
        above), in United States , and other countries.
      </p>
      <p>
        If you are a resident in the European Economic Area, then these
        countries may not necessarily have data protection laws or other similar
        laws as comprehensive as those in your country. We will however take all
        necessary measures to protect your personal information in accordance
        with this privacy notice and applicable law.
      </p>
      <p>European Commission's Standard Contractual Clauses:</p>
      <p>
        We have implemented measures to protect your personal information,
        including by using the European Commission's Standard Contractual
        Clauses for transfers of personal information between our group
        companies and between us and our third-party providers. These clauses
        require all recipients to protect all personal information that they
        process originating from the EEA in accordance with European data
        protection laws and regulations. Our Standard Contractual Clauses can be
        provided upon request. We have implemented similar appropriate
        safeguards with our third-party service providers and partners and
        further details can be provided upon request.
      </p>
      <p>
        <strong>8. HOW LONG DO WE KEEP YOUR INFORMATION?</strong>
      </p>
      <p>
        <strong>In Short: </strong> We keep your information for as long as
        necessary to fulfill the purposes outlined in this privacy notice unless
        otherwise required by law.
      </p>
      <p>
        We will only keep your personal information for as long as it is
        necessary for the purposes set out in this privacy notice, unless a
        longer retention period is required or permitted by law (such as tax,
        accounting or other legal requirements). No purpose in this notice will
        require us keeping your personal information for longer than six (6){" "}
        months past the termination of the user's account .
      </p>
      <p>
        When we have no ongoing legitimate business need to process your
        personal information, we will either delete or anonymize such
        information, or, if this is not possible (for example, because your
        personal information has been stored in backup archives), then we will
        securely store your personal information and isolate it from any further
        processing until deletion is possible.
      </p>
      <p>
        <strong>9. HOW DO WE KEEP YOUR INFORMATION SAFE?</strong>
      </p>
      <p>
        <strong>In Short: </strong> We aim to protect your personal information
        through a system of organizational and technical security measures.
      </p>
      <p>
        We have implemented appropriate technical and organizational security
        measures designed to protect the security of any personal information we
        process. However, despite our safeguards and efforts to secure your
        information, no electronic transmission over the Internet or information
        storage technology can be guaranteed to be 100% secure, so we cannot
        promise or guarantee that hackers, cybercriminals, or other unauthorized
        third parties will not be able to defeat our security, and improperly
        collect, access, steal, or modify your information. Although we will do
        our best to protect your personal information, transmission of personal
        information to and from our Services is at your own risk. You should
        only access the Services within a secure environment.
      </p>
      <p>
        <strong>10. DO WE COLLECT INFORMATION FROM MINORS?</strong>
      </p>
      <p>
        <strong>In Short: </strong> We do not knowingly collect data from or
        market to children under 18 years of age.
      </p>
      <p>
        We do not knowingly solicit data from or market to children under 18
        years of age. By using the Services , you represent that you are at
        least 18 or that you are the parent or guardian of such a minor and
        consent to such minor dependent’s use of the Services . If we learn that
        personal information from users less than 18 years of age has been
        collected, we will deactivate the account and take reasonable measures
        to promptly delete such data from our records. If you become aware of
        any data we may have collected from children under age 18, please
        contact us at abo+fm_platform_dpo@alt-f1.be .
      </p>
      <p>
        <strong>11. WHAT ARE YOUR PRIVACY RIGHTS?</strong>
      </p>
      <p>
        <strong>In Short: </strong> In some regions, such as the European
        Economic Area, you have rights that allow you greater access to and
        control over your personal information. You may review, change, or
        terminate your account at any time.
      </p>
      <p>
        In some regions (like the European Economic Area), you have certain
        rights under applicable data protection laws. These may include the
        right (i) to request access and obtain a copy of your personal
        information, (ii) to request rectification or erasure; (iii) to restrict
        the processing of your personal information; and (iv) if applicable, to
        data portability. In certain circumstances, you may also have the right
        to object to the processing of your personal information. To make such a
        request, please use the <a href="#contact">contact details</a> provided
        below. We will consider and act upon any request in accordance with
        applicable data protection laws.
      </p>
      <p>
        If we are relying on your consent to process your personal information,
        you have the right to withdraw your consent at any time. Please note
        however that this will not affect the lawfulness of the processing
        before its withdrawal, nor will it affect the processing of your
        personal information conducted in reliance on lawful processing grounds
        other than consent.
      </p>
      <p>
        If you are resident in the European Economic Area and you believe we are
        unlawfully processing your personal information, you also have the right
        to complain to your local data protection supervisory authority. You can
        find their contact details here:{" "}
        <a href="https://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm">
          https://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm
        </a>{" "}
        .
      </p>
      <p>
        If you are resident in Switzerland, the contact details for the data
        protection authorities are available here:{" "}
        <a href="https://www.edoeb.admin.ch/edoeb/en/home.html">
          https://www.edoeb.admin.ch/edoeb/en/home.html
        </a>{" "}
        .
      </p>
      <p>
        If you have questions or comments about your privacy rights, you may
        email us at abo+fm_tech_privacy_policy@alt-f1.be .
      </p>
      <p>
        <strong>Account Information</strong>
      </p>
      <p>
        If you would at any time like to review or change the information in
        your account or terminate your account, you can:
      </p>
      <p> ■ Log in to your account settings and update your user account. </p>
      <p> ■ Contact us using the contact information provided. </p>
      <p>
        Upon your request to terminate your account, we will deactivate or
        delete your account and information from our active databases. However,
        we may retain some information in our files to prevent fraud,
        troubleshoot problems, assist with any investigations, enforce our Terms
        of Use and/or comply with applicable legal requirements.
      </p>
      <p>
        <strong>Cookies and similar technologies: </strong> Most Web browsers
        are set to accept cookies by default. If you prefer, you can usually
        choose to set your browser to remove cookies and to reject cookies. If
        you choose to remove cookies or reject cookies, this could affect
        certain features or services of our Services . To opt-out of
        interest-based advertising by advertisers on our Services visit{" "}
        <a href="http://www.aboutads.info/choices/">
          http://www.aboutads.info/choices/
        </a>{" "}
        .{" "}
      </p>
      <p>
        <strong>Opting out of email marketing: </strong> You can unsubscribe
        from our marketing email list at any time by clicking on the unsubscribe
        link in the emails that we send or by contacting us using the details
        provided below. You will then be removed from the marketing email list –
        however, we may still communicate with you, for example to send you
        service-related emails that are necessary for the administration and use
        of your account, to respond to service requests, or for other
        non-marketing purposes. To otherwise opt-out, you may:
      </p>
      <p> ■ Access your account settings and update your preferences. </p>
      <p> ■ Contact us using the contact information provided. </p>
      <p>
        <strong>12. CONTROLS FOR DO-NOT-TRACK FEATURES</strong>
      </p>
      <p>
        Most web browsers and some mobile operating systems and mobile
        applications include a Do-Not-Track (“DNT”) feature or setting you can
        activate to signal your privacy preference not to have data about your
        online browsing activities monitored and collected. At this stage, no
        uniform technology standard for recognizing and implementing DNT signals
        has been finalized. As such, we do not currently respond to DNT browser
        signals or any other mechanism that automatically communicates your
        choice not to be tracked online. If a standard for online tracking is
        adopted that we must follow in the future, we will inform you about that
        practice in a revised version of this privacy notice.
      </p>
      <p>
        <strong>
          13. DO CALIFORNIA RESIDENTS HAVE SPECIFIC PRIVACY RIGHTS?
        </strong>
      </p>
      <p>
        <strong>In Short: </strong> Yes, if you are a resident of California,
        you are granted specific rights regarding access to your personal
        information.
      </p>
      <p>
        California Civil Code Section 1798.83, also known as the “Shine The
        Light” law, permits our users who are California residents to request
        and obtain from us, once a year and free of charge, information about
        categories of personal information (if any) we disclosed to third
        parties for direct marketing purposes and the names and addresses of all
        third parties with which we shared personal information in the
        immediately preceding calendar year. If you are a California resident
        and would like to make such a request, please submit your request in
        writing to us using the contact information provided below.
      </p>
      <p>
        If you are under 18 years of age, reside in California, and have a
        registered account with a Service , you have the right to request
        removal of unwanted data that you publicly post on the Services . To
        request removal of such data, please contact us using the contact
        information provided below, and include the email address associated
        with your account and a statement that you reside in California. We will
        make sure the data is not publicly displayed on the Services , but
        please be aware that the data may not be completely or comprehensively
        removed from all our systems (e.g. backups, etc.).{" "}
      </p>
      <p>
        <strong>CCPA Privacy Notice</strong>
      </p>
      <p>The California Code of Regulations defines a "resident" as:</p>
      <p>
        (1) every individual who is in the State of California for other than a
        temporary or transitory purpose and
      </p>
      <p>
        (2) every individual who is domiciled in the State of California who is
        outside the State of California for a temporary or transitory purpose
      </p>
      <p>All other individuals are defined as "non-residents."</p>
      <p>
        If this definition of "resident" applies to you, certain rights and
        obligations apply regarding your personal information.
      </p>
      <p>
        <strong>What categories of personal information do we collect?</strong>
      </p>
      <p>
        We have collected the following categories of personal information in
        the past twelve (12) months:{" "}
      </p>
      <table>
        <tbody>
          <tr>
            <td>
              <strong>Category</strong>
            </td>
            <td>
              <strong>Examples</strong>
            </td>
            <td>
              <strong>Collected</strong>
            </td>
          </tr>
          <tr>
            <td>A. Identifiers</td>
            <td>
              Contact details, such as real name, alias, postal address,
              telephone or mobile contact number, unique personal identifier,
              online identifier, Internet Protocol address, email address and
              account name
            </td>
            <td>
              <p>YES</p>
            </td>
          </tr>
          <tr>
            <td>
              B. Personal information categories listed in the California
              Customer Records statute
            </td>
            <td>
              Name, contact information, education, employment, employment
              history and financial information
            </td>
            <td>YES</td>
          </tr>
          <tr>
            <td>
              C. Protected classification characteristics under California or
              federal law
            </td>
            <td>Gender and date of birth</td>
            <td>
              <p>YES</p>
            </td>
          </tr>
          <tr>
            <td>D. Commercial information</td>
            <td>
              Transaction information, purchase history, financial details and
              payment information
            </td>
            <td>
              <p>YES</p>
            </td>
          </tr>
          <tr>
            <td>E. Biometric information</td>
            <td>Fingerprints and voiceprints</td>
            <td>
              <p>NO</p>
            </td>
          </tr>
          <tr>
            <td>F. Internet or other similar network activity</td>
            <td>
              Browsing history, search history, online behavior, interest data,
              and interactions with our and other websites, applications,
              systems and advertisements
            </td>
            <td>
              <p>YES</p>
            </td>
          </tr>
          <tr>
            <td>G. Geolocation data</td>
            <td>Device location</td>
            <td>
              <p>YES</p>
            </td>
          </tr>
          <tr>
            <td>
              H. Audio, electronic, visual, thermal, olfactory, or similar
              information
            </td>
            <td>
              Images and audio, video or call recordings created in connection
              with our business activities
            </td>
            <td>
              <p>YES</p>
            </td>
          </tr>
          <tr>
            <td>I. Professional or employment-related information</td>
            <td>
              Business contact details in order to provide you our services at a
              business level, job title as well as work history and professional
              qualifications if you apply for a job with us
            </td>
            <td>
              <p>NO</p>
            </td>
          </tr>
          <tr>
            <td>J. Education Information</td>
            <td>Student records and directory information</td>
            <td>
              <p>NO</p>
            </td>
          </tr>
          <tr>
            <td>K. Inferences drawn from other personal information</td>
            <td>
              Inferences drawn from any of the collected personal information
              listed above to create a profile or summary about, for example, an
              individual’s preferences and characteristics
            </td>
            <td>NO</td>
          </tr>
        </tbody>
      </table>
      <p>
        We may also collect other personal information outside of these
        categories in instances where you interact with us in-person, online, or
        by phone or mail in the context of:
      </p>
      <ul>
        <li>Receiving help through our customer support channels</li>
        <li>Participation in customer surveys or contests; and</li>
        <li>
          Facilitation in the delivery of our Services and to respond to your
          inquiries
        </li>
      </ul>
      <p>
        <strong>How do we use and share your personal information?</strong>
      </p>
      <p>ALT-F1 SPRL collects and shares your personal information through:</p>
      <ul>
        <li>Targeting cookies/Marketing cookies</li>
      </ul>
      <ul>
        <li>Social media cookies</li>
      </ul>
      <ul>
        <li>Beacons/Pixels/Tags</li>
      </ul>
      <p>
        More information about our data collection and sharing practices can be
        found in this privacy notice .
      </p>
      <p>
        You can opt out from the selling of your personal information by
        disabling cookies in the Cookies Preferences Settings or clicking on the
        Do Not Sell My Personal Information link on our homepage.
      </p>
      <p>
        You may contact us by email at abo+fm_platform_ccpa_inquiry@alt-f1.be ,{" "}
        by visiting <a href="http://www.alt-f1.be">http://www.alt-f1.be</a> , or
        by referring to the contact details at the bottom of this document.
      </p>
      <p>
        If you are using an authorized agent to exercise your right to opt-out,
        we may deny a request if the authorized agent does not submit proof that
        they have been validly authorized to act on your behalf.
      </p>
      <p>
        <strong>Will your information be shared with anyone else?</strong>
      </p>
      <p>
        We may disclose your personal information with our service providers
        pursuant to a written contract between us and each service provider.
        Each service provider is a for-profit entity that processes the
        information on our behalf.
      </p>
      <p>The current list of our service providers can be found below.</p>
      <p>
        We may use your personal information for our own business purposes, such
        as for undertaking internal research for technological development and
        demonstration. This is not considered to be "selling" of your personal
        data.
      </p>
      <p>
        ALT-F1 SPRL has disclosed the following categories or personal
        information to third parties for a business or commercial purpose in the
        preceding twelve (12) months:
      </p>
      <ul>
        <li>
          Category A. Identifiers, such as contact details, like your real name,
          alias, postal address, telephone or mobile contact number, unique
          personal identifier, online identifier, Internet Protocol address,
          email address and account name.
        </li>
      </ul>
      <ul>
        <li>
          Category B. Personal information, as defined in the California
          Customer Records law, such as your name, contact information,
          education, employment, employment history and financial information.
        </li>
      </ul>
      <ul>
        <li>
          Category C. Characteristics of protected classifications under
          California or federal law, such as gender or date of birth.
        </li>
      </ul>
      <ul>
        <li>
          Category D. Commercial information, such as transaction information,
          purchase history, financial details and payment information.
        </li>
      </ul>
      <ul>
        <li>
          Category F. Internet or other electronic network activity information,
          such as browsing history, search history, online behavior, interest
          data, and interactions with our and other websites, applications,
          systems and advertisements.
        </li>
      </ul>
      <ul>
        <li>Category G. Geolocation data, such as device location.</li>
      </ul>
      <ul>
        <li>
          Category H. Audio, electronic, visual and similar information, such as
          images and audio, video or call recordings created in connection with
          our business activities.
        </li>
      </ul>
      <p>
        The categories of third parties to whom we disclosed personal
        information for a business or commercial purpose can be found under "{" "}
        <a href="#whoshare">WHO WILL YOUR INFORMATION BE SHARED WITH?</a> ".
      </p>
      <p>
        ALT-F1 SPRL has sold the following categories of personal information to
        third parties in the preceding twelve (12) months:
      </p>
      <ul>
        <li>
          Category B. Personal information, as defined in the California
          Customer Records law, such as your name, contact information,
          education, employment, employment history and financial information.
        </li>
      </ul>
      <p>
        The categories of third parties to whom we sold personal information
        are:
      </p>
      <p>
        <strong>Your rights with respect to your personal data</strong>
      </p>
      <p>Right to request deletion of the data - Request to delete</p>
      <p>
        You can ask for the deletion of your personal information. If you ask us
        to delete your personal information, we will respect your request and
        delete your personal information, subject to certain exceptions provided
        by law, such as (but not limited to) the exercise by another consumer of
        his or her right to free speech, our compliance requirements resulting
        from a legal obligation or any processing that may be required to
        protect against illegal activities.
      </p>
      <p>Right to be informed - Request to know</p>
      <p>Depending on the circumstances, you have a right to know:</p>
      <ul>
        <li>
          <p>whether we collect and use your personal information;</p>
        </li>
        <li>
          <p>the categories of personal information that we collect;</p>
        </li>
        <li>
          <p>
            the purposes for which the collected personal information is used;
          </p>
        </li>
        <li>
          <p>whether we sell your personal information to third parties;</p>
        </li>
        <li>
          <p>
            the categories of personal information that we sold or disclosed for
            a business purpose;
          </p>
        </li>
        <li>
          <p>
            the categories of third parties to whom the personal information was
            sold or disclosed for a business purpose; and
          </p>
        </li>
        <li>
          <p>
            the business or commercial purpose for collecting or selling
            personal information.
          </p>
        </li>
      </ul>
      <p>
        In accordance with applicable law, we are not obligated to provide or
        delete consumer information that is de-identified in response to a
        consumer request or to re-identify individual data to verify a consumer
        request.
      </p>
      <p>
        Right to Non-Discrimination for the Exercise of a Consumer's Privacy
        Rights
      </p>
      <p>
        We will not discriminate against you if you exercise your privacy
        rights.
      </p>
      <p>Verification process</p>
      <p>
        Upon receiving your request, we will need to verify your identity to
        determine you are the same person about whom we have the information in
        our system. These verification efforts require us to ask you to provide
        information so that we can match it with the information you have
        previously provided us. For instance, depending on the type of request
        you submit, we may ask you to provide certain information so that we can
        match the information you provide with the information we already have
        on file, or we may contact you through a communication method (e.g.
        phone or email) that you have previously provided to us. We may also use
        other verification methods as the circumstances dictate.
      </p>
      <p>
        We will only use personal information provided in your request to verify
        your identity or authority to make the request. To the extent possible,
        we will avoid requesting additional information from you for the
        purposes of verification. If, however, if we cannot verify your identity
        from the information already maintained by us, we may request that you
        provide additional information for the purposes of verifying your
        identity, and for security or fraud-prevention purposes. We will delete
        such additionally provided information as soon as we finish verifying
        you.
      </p>
      <p>Other privacy rights</p>
      <ul>
        <li>
          <p>you may object to the processing of your personal data</p>
        </li>
        <li>
          <p>
            you may request correction of your personal data if it is incorrect
            or no longer relevant, or ask to restrict the processing of the data
          </p>
        </li>
        <li>
          <p>
            you can designate an authorized agent to make a request under the
            CCPA on your behalf. We may deny a request from an authorized agent
            that does not submit proof that they have been validly authorized to
            act on your behalf in accordance with the CCPA.
          </p>
        </li>
        <li>
          <p>
            you may request to opt-out from future selling of your personal
            information to third parties. Upon receiving a request to opt-out,
            we will act upon the request as soon as feasibly possible, but no
            later than 15 days from the date of the request submission.
          </p>
        </li>
      </ul>
      <p>
        To exercise these rights, you can contact us by email at{" "}
        abo+fm_platform_ccpa_inquiry@alt-f1.be , by visiting{" "}
        <a href="http://www.alt-f1.be">http://www.alt-f1.be</a> , or by
        referring to the contact details at the bottom of this document. If you
        have a complaint about how we handle your data, we would like to hear
        from you.
      </p>
      <p>
        <strong>14. DO WE MAKE UPDATES TO THIS NOTICE?</strong>
      </p>
      <p>
        <strong>In Short: </strong> Yes, we will update this notice as necessary
        to stay compliant with relevant laws.
      </p>
      <p>
        We may update this privacy notice from time to time. The updated version
        will be indicated by an updated “Revised” date and the updated version
        will be effective as soon as it is accessible. If we make material
        changes to this privacy notice, we may notify you either by prominently
        posting a notice of such changes or by directly sending you a
        notification. We encourage you to review this privacy notice frequently
        to be informed of how we are protecting your information.
      </p>
      <p>
        <strong>15. HOW CAN YOU CONTACT US ABOUT THIS NOTICE?</strong>
      </p>
      <p>
        If you have questions or comments about this notice, you may contact our
        Data Protection Officer (DPO), Abdelkrim BOUJRAF , by email at{" "}
        abo+fm_platform_dpo@alt-f1.be , by phone at +32 497 480 970 , or by post
        to:
      </p>
      <p>ALT-F1 SPRL </p>
      <p>Abdelkrim BOUJRAF</p>
      <p>Drootbeekstraat 13</p>
      <p>Brussels 1020 </p>
      <p>Belgium</p>
      <p>
        <strong>
          HOW CAN YOU REVIEW, UPDATE, OR DELETE THE DATA WE COLLECT FROM YOU?
        </strong>
      </p>
      <p>
        Based on the applicable laws of your country, you may have the right to
        request access to the personal information we collect from you, change
        that information, or delete it in some circumstances. To request to
        review, update, or delete your personal information, please visit:{" "}
        <a href="http://www.alt-f1.be">http://www.alt-f1.be</a> . We will
        respond to your request within 30 days.
      </p>
      <p>
        This privacy policy was created using{" "}
        <a href="https://termly.io/products/privacy-policy-generator/?ftseo">
          Termly’s Privacy Policy Generator
        </a>{" "}
        .
      </p>
    </>
  );
}
