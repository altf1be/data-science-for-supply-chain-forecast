const { GoogleSpreadsheet } = require('google-spreadsheet');

import * as dotenv from 'dotenv'
dotenv.config()

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    try {
        //https://docs.google.com/spreadsheets/d/1kovOQzAQiwA2IqCJ0LraguaXLRY8_iNL5p9lVExFQUw/edit#gid=0
        const doc = new GoogleSpreadsheet('1kovOQzAQiwA2IqCJ0LraguaXLRY8_iNL5p9lVExFQUw');
        await doc.useServiceAccountAuth({
            // env var values are copied from service account credentials generated by google
            // see "Authentication" section in docs for more info
            client_email: process.env.CLIENT_EMAIL,
            private_key: process.env.PRIVATE_KEY,

        });
        const sheetInfo = await doc.loadInfo();
        console.log(doc.title);
         await doc.updateProperties({ title: 'doc' });
        const sheet = await doc.sheetsByTitle['stats'];
         // or use doc.sheetsById[id] or doc.sheetsByTitle[title]
        const rows = await sheet.getRows();
        const title= await sheet.title
      
        // adding / removing sheets
       // const newSheet = await doc.addSheet({ title: 'hot new sheet!' });
    
        res.status(200).json(title)
    } catch (err) {
        return res.status(400).json( err )

    }
}