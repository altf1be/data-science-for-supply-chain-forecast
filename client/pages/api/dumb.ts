import { google } from "googleapis";
// import { docs } from "googleapis/build/src/apis/docs";
import { NextApiRequest, NextApiResponse } from "next";
import path from "path";

import * as dotenv from 'dotenv'
dotenv.config()

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    try {

        // const auth = await new google.auth.GoogleAuth({
        //     credentials: {
        //         client_email: key.client_email,
        //         private_key: key.private_key?.replace(/\\n/g, '\n')
        //     },
        //     scopes: [
        //         'https://www.googleapi.com/auth/drive',
        //         'https://www.googleapi.com/auth/drive.file',
        //         'https://www.googleapi.com/auth/spreadsheets',
        //     ]
        // });
       

        const auth = new google.auth.GoogleAuth({
        credentials: {
            client_email: process.env.CLIENT_EMAIL,
            private_key: process.env.PRIVATE_KEY.replace(/\\n/g, '\n'),
        },
            scopes: [
                         'https://www.googleapi.com/auth/drive',
                         'https://www.googleapi.com/auth/drive.file',
                         'https://www.googleapi.com/auth/spreadsheets',
                     ]
          });
      // const client = await auth.getClient();
        const gsapi = google.sheets({
            auth,
            version: 'v4'
        });
    //    return res.status(200).json(client)
       
        // https://docs.google.com/spreadsheets/d/1wUcPGIaJ8axvgrSI6CpjHXJye1x3bLAVz4463Qp3RQc/edit#gid=0
        // https://docs.google.com/spreadsheets/d/19NGhx7nhzyNDQsuFCudZG9aYiw9os3JhHJxaalvR6RU/edit#gid=0
        const opt = {
            spreadsheetId: '19NGhx7nhzyNDQsuFCudZG9aYiw9os3JhHJxaalvR6RU',
            range: 'stats!A2:A'
        }

        let data = await gsapi.spreadsheets.values.get(opt);
        console.log({ data: data.data.values })
        return res.status(200).json(data)

        // client.authorize(async function (err, tokens) {
        //     if (err) {
        //         return res.status(400).send(JSON.stringify({ error: true }));
        //     }

        //     // https://docs.google.com/spreadsheets/d//edit#gid=0
        //     const gsapi = google.sheets({ version: 'v4', auth: client });
        //     const opt = {
        //         spreadsheetId: '1wUcPGIaJ8axvgrSI6CpjHXJye1x3bLAVz4463Qp3RQc',
        //         range: 'sheet1!A2:A'
        //     }

        //     let data = await gsapi.spreadsheets.values.get(opt);
        //     console.log({data: data.data.values})
        //     return res.status(200).send(JSON.stringify({ error: false, data: data.data.values }));

        // })

    } catch (err) {
        return res.status(400).send({ err })

    }
}