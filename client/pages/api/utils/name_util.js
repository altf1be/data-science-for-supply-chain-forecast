import { v4 as uuidv4 } from 'uuid';

 const createName= ()=>{

    var now = new Date();
// Create formatted time
var yyyy = now.getFullYear();
var mm = now.getMonth() < 9 ? "0" + (now.getMonth() + 1) : now.getMonth() + 1; // getMonth() is zero-based
var dd = now.getDate() < 10 ? "0" + now.getDate() : now.getDate();
var hh = now.getHours() < 10 ? "0" + now.getHours() : now.getHours();
var mmm = now.getMinutes() < 10 ? "0" + now.getMinutes() : now.getMinutes();
var ss = now.getSeconds() < 10 ? "0" + now.getSeconds() : now.getSeconds();

// file path with / at the end
var path = "/home/pi/node-red-static/"; // This is the path
var filename =
  yyyy +
  "-" +
  mm +
  "-" +
  dd +
  "_" +
  hh +
  "h" +
  mm +
  "m" +
  ss +
  "s" +
  "-" +
  `${uuidv4()}`; // file name

  return filename
}


export default createName;