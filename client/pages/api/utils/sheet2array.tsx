import { read, utils, writeFile } from "xlsx";

const sheet2arr = (sheet) => {
  var result = [];
  var row;
  var rowNum;
  var colNum;
  var range = utils.decode_range(sheet["!ref"]);
  for (rowNum = range.s.r; rowNum <= range.e.r; rowNum++) {
    row = [];
    for (colNum = range.s.c; colNum <= range.e.c; colNum++) {
      var nextCell = sheet[utils.encode_cell({ r: rowNum, c: colNum })];
      if (typeof nextCell === "undefined") {
        row.push(void 0);
      } else row.push(nextCell.w);
    }
    result.push(row);
  }

//   setDataArray(result);
//   setLabels(result[0]);
  return result;
};
export default sheet2arr;
