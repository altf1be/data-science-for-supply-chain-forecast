const getDataSets = (dataArray) => {



  // function getData(data) {
  //   console.log(data);
  //   const cols_length = data[0].length;
  //   let cols = {};
  //   '{col[n]:[]}'
  //   data[0].map((el, i) => Object.assign(cols, { [i]: [] }));

  //   data.map((el1, i1) => {
  //     el1.filter((el2, i2) =>{
  //        if ((i1> 0) && (i2> 0) ){
  //         cols[i2].push(el2)
  //       }  
  //     }
  //   //  typeof el2 !== 'undefined' ? cols[`col${i2}`].push(el2) : cols[`col${i2}`].push(Number.NaN)

  //     );
  //     // }
  //   });
  // console.log('getDAta')
  //   console.log(cols);

  //   return cols;
  // }

  // const datasetsArr = [];


  // const  hexColor= {
  //     red:"#FF2D00",
  //     blue:"#1ab394",
  //     lightBlue:"#CBCBCB"

  //   }

  //   let labels=dataArray[0]
  //   dataArray.splice(0,1)
  //   let dataMatrix=dataArray.map(el=> el.slice(1,el.length))

  //       const data = {
  //         fill: false,
  //         spanGaps:false,
  //         // label: value[0],
  //         data: [...dataMatrix],
  //         // borderColor:( value[0] === 'Demand'? hexColor.blue:value[0] === 'Forecast'? hexColor.lightBlue: value[0] === 'Error'? hexColor.red:null),
  //         // backgroundColor: this.borderColor.concat("80"),
  //       };
  //       // data.backgroundColor= data.borderColor.concat("80");

  //       datasetsArr.push(data);

  // return datasetsArr;





  function getData(data) {
    console.log(data);
    const cols_length = data[0].length;
    let cols = {};
    data[0].map((el, i) => Object.assign(cols, { [`col${i}`]: [] }));

    data.map((el1, i1) => {
      el1.filter((el2, i2) =>
        el2 !== undefined ? cols[`col${i2}`].push(el2) : cols[`col${i2}`].push(null)
      );
      // }
    });

    console.log(cols);

    return cols;
  }

  const datasetsArr = [];


  const hexColor = {
    red: "#FF2D00",
    blue: "#1ab394",
    lightBlue: "#CBCBCB"

  }



  if (dataArray && dataArray.length > 0) {
    for (let [col, value] of Object.entries(getData(dataArray))) {
      if (col !== 'col0') {

        const data = {
          showLine : true,
          pointRadius : 0,
          cubicInterpolationMode: 'monotone',
          fill: false,
          spanGaps:false,
          label: value[0],
          data: [...value.splice(1)],
          // data: [...value.map((el, i) => (i > 0 ? el : null))],
          borderColor: (value[0] === 'Demand' ? hexColor.blue : value[0] === 'Forecast' ? hexColor.lightBlue : value[0] === 'Error' ? hexColor.red : null),
          // backgroundColor: this.borderColor.concat("80"),
        };
        data.backgroundColor = data.borderColor.concat("80");

        datasetsArr.push(data);
      }


    }



  }
  return datasetsArr;


};


export default getDataSets;