import { read, utils, writeFile } from "xlsx";
import getDataSets from "./data_set_manager";
import sheet2arr from "./sheet2array";


const read_spreadsheet = (sheet:Buffer) => {
   const wb = read(sheet, { type: "buffer",});
   let result = sheet2arr(wb.Sheets[wb.SheetNames[1]]);

   const labels = [...result.map((el,i)=> i!== 0 ? el[0]:null).splice(1)];
   const datasets = [...getDataSets(result)]

   return {
      labels,
      datasets,
   }
      ;
}



export default read_spreadsheet;