// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import axios, {AxiosResponse }from 'axios'
interface Data {data: number[]}
interface responseData {
  results: Data[]
}

let data: Data;
export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {

let results =axios.get<AxiosResponse>('http://localhost:5000/charts/2').then(resp=>{
  console.log({fromlineChart:resp})
  data= resp.data
})



res.status(200).json(data)
}
