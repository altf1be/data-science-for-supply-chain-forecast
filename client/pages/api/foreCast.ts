import * as dotenv from 'dotenv'
import type { NextApiRequest, NextApiResponse } from 'next'
import { IncomingForm } from 'formidable'
import fs from 'fs'
import axios from 'axios';
import FormData from 'form-data'
import NextCors from './utils/nextCors';
// import { buffer } from 'stream/consumers';

export const config = {
  api: {
    bodyParser: false,
  },
};
dotenv.config()
 
export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  // let file = {}
 
  await NextCors(req, res, {
    // Options
    methods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE'],
    origin: '*',
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
 });
  const form = new IncomingForm({keepExtensions:true});
  if (req.method === 'POST') {

    try {
      // let tmpObj = tmp.dirSync({ unsafeCleanup: true });
      form.parse(req, function (err, fields, files) {
        if (err) return  err
        const formData = new FormData();
        const stream =  fs.createReadStream(files.file.filepath)
        formData.append("file", stream);
        axios.post(`${process.env.API_URI}`, formData, {
          headers: {
            ...formData.getHeaders(),
            'Content-Type': 'blob',
            
          },
          withCredentials:false,
          responseType:"arraybuffer"
        }).then(response => {
          // const buffer = Buffer.from(response.data, 'base64');
           res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
           res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
          // res.setHeader('Content-Disposition', 'attachment;filename=SheetJSNode.xlsx')
          res.status(200).send(response.data)
        }).catch(err => {
          console.log(err)
        })
      });


      //       var config = {
      //         method: 'post',
      //         url: 'https://38a7-85-28-69-135.eu.ngrok.io/forecast/movingaverage',
      //         headers: { 
      //           ...formData.getHeaders(),

      //         },

      //         data : formData
      //       };

      //       axios(config)
      // .then(function (response) {
      //   console.log(JSON.stringify(response.data));
      //    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
      //   res.send(response.data)
      // })
      // .catch(function (error) {
      //   console.log(error);
      // });


    } catch (err) {
      //  res.status(err.response.status).send(err)
      console.log(err)
    }
  } else {
    // Handle any other HTTP method
  }

}