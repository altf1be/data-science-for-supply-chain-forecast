import React, { useEffect } from "react";
import Head from "next/head";


export async function getServerSideProps() {
  console.info({
    props: {},
  });
  return {
    props: {},
  };
}

function App({}) {
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <link rel="manifest" href="/manifest.json" />
        <title>Home - Data Science For Supply Chain Forecasting</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Head>
      <div className="row wrapper border-bottom white-bg page-heading">
        <div className="col-lg-10">
          <h2>Welcome</h2>
        </div>
        <div className="col-lg-2"></div>
      </div>

      <div className="wrapper wrapper-content animated fadeInRight">
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox ">
              <div className="ibox-content text-center p-md">
                <h3 className="text-navy">
                  Data Science for Supply Chain Forecasting
                </h3>
              </div>
            </div>
       
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
