import Document, { DocumentContext, DocumentInitialProps } from "next/document";
import { Html, Head, Main, NextScript } from "next/document";
import Script from "next/script";

class MyDocument extends Document {
  static async getInitialProps(
    ctx: DocumentContext
  ): Promise<DocumentInitialProps> {
    const initialProps = await Document.getInitialProps(ctx);

    return initialProps;
  }
  render() {
    return (
      <Html lang="en">
        <Head />
        <link rel="shortcut icon" href="/static/favicon.ico" />
        <link rel="stylesheet" href="../styles/css/bootstrap.min.css" />
        <link rel="stylesheet" href="../styles/css/bootstrap.css" />
        <link
          rel="stylesheet"
          href="../styles/css/plugins/fontawesome/5.15.1/css/fontawesome.css"
        />
        {/* <link rel="stylesheet" href="../styles/css/plugins/fontawesome/5.15.1/css/font-awesome.css" /> */}
        <link
          rel="stylesheet"
          href="../styles/css/plugins/fontawesome/5.15.1/css/fontawesome.css"
        />
        <link rel="stylesheet" href="../styles/css/animate.css" />
        <link rel="stylesheet" href="../styles/css/style.css" />
        <link rel="stylesheet" href="../styles/css/style.css" />
        <link rel="stylesheet" href="../styles/css/sweetalert.css" />
        <link rel="stylesheet" href="../styles/css/jquery.steps.css" />
        <body>
          <Main />
          <NextScript />
          {/* <Script
            src="/styles/js/plugins/jquery/3.1.1/jquery.min.js"
            strategy="beforeInteractive"
          /> */}
          <Script
            src="/styles/js/jquery-3.1.1.min.js"
            strategy="beforeInteractive"
          />
          <Script
            src="/styles/js/popper.min.js"
            strategy="beforeInteractive"
          ></Script>
          <Script
            src="/styles/js/bootstrap.min.js"
            strategy="beforeInteractive"
          ></Script>
          <Script
            src="/styles/js/plugins/metisMenu/jquery.metisMenu.js"
            strategy="beforeInteractive"
          ></Script>
          <Script
            src="/styles/js/plugins/slimscroll/jquery.slimscroll.min.js"
            strategy="beforeInteractive"
          ></Script>
          <Script
            src="/styles/js/jquery.steps.min.js"
            strategy="beforeInteractive"
          ></Script>

          <Script
            src="/styles/js/inspinia.js"
            strategy="beforeInteractive"
          ></Script>
          <Script
            src="/styles/js/plugins/pace/pace.min.js"
            strategy="beforeInteractive"
          ></Script>
          <Script
            src="/styles/js/sweetalert.min.js"
            strategy="beforeInteractive"
          ></Script>
          <Script
            src="/styles/js/chart.min-4.1.1.js"
            strategy="beforeInteractive"
          ></Script>
        </body>
      </Html>
    );
  }
}

export default MyDocument;
