export const htmlMetaTags = [
  {
    meta: "keywords",
    content: "api, sca-tork, easycube, IoT, dispensers",
  },
  {
    meta: "og:title",
    content: "Facility Management Platform - FM Tech",
  },
];
