import { useEffect, useState, memo } from "react";
import { read, utils, writeFile } from "xlsx";
import Script from "next/script";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  ArcElement,
  Filler,
  RadialLinearScale,
  LineController,
  BarElement,
  BarController,
} from "chart.js";
import { Line } from "react-chartjs-2";

ChartJS.register(
  Filler,
  LineController,
  BarElement,
  BarController,
  RadialLinearScale,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  ArcElement
);

function ChartContainer({ chartData }: any) {
  const options = {
    scales: {
      x: {
        title:{
          display: true,
          text: " Periods"
        }
      },
      y:{
        grid:{
          lineWidth: (ctx)=>ctx.tick.value === 0 ? 3: null,
          color:(ctx)=>ctx.tick.value === 0 ? "black": null,
        }
      }
  },
  interaction: {
    intersect: false,
    mode: 'index',
},
plugins : {
  tooltip : {
    callbacks : {
      label : function (ctx) {
        let lab = ctx.dataset.label + ": " + ctx.dataset.data[ctx.dataIndex].labelText + " ~ " + ctx.dataset.data[ctx.dataIndex].y;
        return lab;
      },
      title: function(ctx) {
          return "Differenza";
       }
    }
  }
},
    responsive: true,
    plugins: {
     
      legend: {
        position: "top" as const,
      },
      title: {
        display: true,
        text: "Forecast Line Chart",
      },
    },
  };
  useEffect(() => {



    const ctx = document.getElementById("lineChart").getContext("2d");
    new Chart(ctx, {type: 'line', data: chartData, options:options});
  }, [chartData]);

  return (
    <>
      {/* <div className="col-lg-6">
      <div className="ibox ">
        <div className="ibox-content">
          <div>
            <Line id="lineChart" options={options} data={chartData} />
          </div>
        </div>
      </div>
      
    </div> */}

      {/* <div className="wrapper wrapper-content animated fadeInRight"> */}
       
      <div className="col-lg-6">
                    <div className="ibox ">
                        <div className="ibox-title">
                            <h5> Moving Average Forecast Chart</h5>
                        </div>
                        <div className="ibox-content">
                            <div>
                                <canvas id="lineChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
      {/* </div> */}
    </>
  );
}

export default ChartContainer;
