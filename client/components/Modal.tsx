import { useState, useEffect } from "react";
import Image from "next/image";
import spreadsheet_img from "../public/Screenshot.jpg"

interface ModalContent{
    title:string,
    sub_title:string,
    body:string,
    img_src:string
    btn_text: string
}

const text_style={
    fontSize: "12px",
  textTransform: "uppercase",
  fontFamily: "'Gambetta', serif",
  letterSpacing: "",
  transition: "700ms ease",
  fontVariationSettings: "wght 311",
  marginBottom: "0.8rem",
  color: "white",
//   backgroundColor:"#145369 !important",
  outline: "none",
  textAlign: "center",
}
export default function Modal({
     title,
     sub_title,
     body,
     img_src,
     btn_text,
     ...props}:ModalContent) {
 
    return (
        <>
         <button style={text_style} type="button" className="btn btn-primary" data-toggle="modal" data-target="#myModal4">
                               Check spreadsheet acceptable format or downlaod our instance
                            </button>
                            <div className="modal inmodal" id="myModal4" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div className="modal-dialog">
                                <div className="modal-content animated fadeIn">
                                    <div className="modal-header">
                                        <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span className="sr-only">Close</span></button>
                                        <i onMouseOver={(e)=>e?e.target.style.backgroundColor='#1fb394':''}
                                        onMouseLeave={(e)=>e?e.target.style.backgroundColor='':''}
                                        className={`fa fa-info-circle modal-icon`}></i>
                                        <h4 className="modal-title">{title}</h4>
                                        <small>{sub_title}</small>
                                    </div>
                                    <div className="modal-body">

                                    <Image src={spreadsheet_img} className="img-fluid" alt="Responsive image"></Image>
                                        {/* <p><strong>Lorem Ipsum is simply dummy</strong> text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
                                            printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
                                            remaining essentially unchanged.</p> */}
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-white" data-dismiss="modal">Close</button>
                                        <button onClick={()=>{
                                            const link = document.createElement("a");
                                            link.download = name;
                                            link.href = props.spreadsheet;
                                            link.click();
                                        }} type="button" className="btn btn-primary">{btn_text}</button>
                                    </div>
                                </div>
                            </div>
                        </div></>
       
    )
}
