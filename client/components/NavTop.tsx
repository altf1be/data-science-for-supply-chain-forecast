// import navStyles from "../styles/Nav.module.css";
import Link from "next/link";
const NavTop = () => {
  return (
    <nav
      className="navbar navbar-static-top white-bg"
      role="navigation"
      // style="margin-bottom: 0"
    >
      <div className="navbar-header">
        <a
          className="navbar-minimalize minimalize-styl-2 btn btn-primary "
          href="#"
        >
          <i className="fa fa-bars"></i>{" "}
        </a>
        <form
          role="search"
          className="navbar-form-custom"
          method="post"
          action="#"
        >
          <div className="form-group">
            <input
              type="text"
              placeholder="Search for something..."
              className="form-control"
              name="top-search"
              id="top-search"
            />
          </div>
        </form>
      </div>
      <ul className="nav navbar-top-links navbar-right">
        <li>
          <a href="#">
            <i className="fas fa-sign-out-alt"></i> Log out
          </a>
        </li>
      </ul>
    </nav>
  );
};

export default NavTop;
