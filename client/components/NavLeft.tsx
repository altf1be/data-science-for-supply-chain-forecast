import Link from "next/link";
import { Router, useRouter } from "next/router";
import { useState, useEffect, useRef } from "react";

const NavLeft = () => {
  const router = useRouter();
  const [menuShow, setMenuShow] = useState<Boolean>();

  console.log({ path: router.asPath });
  // const handleClick = event => {

  //   // event.currentTarget.classNameList.toggle('fa-cheveron-down');
  //   // console.log(event.currentTarget.childNodes[0].childNodes[4].classNameList)
  //   setMenuShow(!menuShow)

  // };

  const goTo = (path: string) => {
    router.push(path);
  };
  useEffect(() => {
    console.log({ menuShow });
  }, [menuShow]);
  return (
    <>
      <nav className="navbar-default navbar-static-side" role="navigation">
        <div className="sidebar-collapse">
          <ul className="nav metismenu" id="side- menu">
            {/* <li className={router.pathname === "/" ? "active" : ""}>
              <a
                href=""
                onClick={(e) => {
                  e.preventDefault();
                  goTo("/");
                }}
              >
                <i className="fas fa-home"></i>{" "}
                <span className="nav-label">Dashboard</span>
              </a>
            </li> */}

            <li className={router.pathname === "/about" ? "active" : ""}>
              <a
                href=""
                onClick={(e) => {
                  e.preventDefault();
                  goTo("/forecast/movingaverage");
                }}
              >
                <i className="fas fa-book" aria-hidden="true"></i>{" "}
                <span className="nav-label">Moving Average</span>{" "}
              </a>
            </li>

            {/* <li className={router.pathname === "/about" ? "active" : ""}>
              <a
                href=""
                onClick={(e) => {
                  e.preventDefault();
                  goTo("/about");
                }}
              >
                <i className="fas fa-info-circle" aria-hidden="true"></i>{" "}
                <span className="nav-label">About</span>{" "}
              </a>
            </li> */}
          </ul>
        </div>
      </nav>
    </>
  );
};

export default NavLeft;
