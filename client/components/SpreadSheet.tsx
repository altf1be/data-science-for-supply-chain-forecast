import  {useState, useRef, useEffect } from "react";
import jspreadsheet from "jspreadsheet";
import "/node_modules/jspreadsheet/dist/jspreadsheet.css";
import "/node_modules/jsuites/dist/jsuites.css";
import "jspreadsheet/dist/jspreadsheet.css";
import "jsuites/dist/jsuites.css";
// import importer from "../node_modules/@jspreadsheet/importer/dist/index";
import parser from "../node_modules/@jspreadsheet/parser";

  
jspreadsheet.setLicense(
    "ZjI5NWE5YmI0YzZiMjZmODRkMTg2Y2RlZDNlZjQzYWUxMTk0N2Q3NzRkYmRhZTBhOTQyNGQ3YmUwZjVlZjUzZTQzZjJmNzE3MjliNTZiMzQwZjU0YTczMWI4ZjQ3MWYzZmI3MDc5YTBmZTc3NzhjY2I0ZmZiOWVmZWYwMDRlMzksZXlKdVlXMWxJam9pUkVKZlpYaHdaWEp0YVc1MGFXNW5JaXdpWkdGMFpTSTZNVFkzTVRBMk1qUXdNQ3dpWkc5dFlXbHVJanBiSWlJc0lteHZZMkZzYUc5emRDSmRMQ0p3YkdGdUlqb3dMQ0p6WTI5d1pTSTZXeUoyTnlJc0luWTRJaXdpZGpraVhYMD0="
  );
  jspreadsheet.setExtensions({ parser });
  

export default function SpreadSheet({sheet}:any) {


  jspreadsheet.setLicense(
    "ZjI5NWE5YmI0YzZiMjZmODRkMTg2Y2RlZDNlZjQzYWUxMTk0N2Q3NzRkYmRhZTBhOTQyNGQ3YmUwZjVlZjUzZTQzZjJmNzE3MjliNTZiMzQwZjU0YTczMWI4ZjQ3MWYzZmI3MDc5YTBmZTc3NzhjY2I0ZmZiOWVmZWYwMDRlMzksZXlKdVlXMWxJam9pUkVKZlpYaHdaWEp0YVc1MGFXNW5JaXdpWkdGMFpTSTZNVFkzTVRBMk1qUXdNQ3dpWkc5dFlXbHVJanBiSWlJc0lteHZZMkZzYUc5emRDSmRMQ0p3YkdGdUlqb3dMQ0p6WTI5d1pTSTZXeUoyTnlJc0luWTRJaXdpZGpraVhYMD0="
  );
  jspreadsheet.setExtensions({ parser });
 

  useEffect(() => {
    jspreadsheet.parser({
        file:  sheet,
        onload: function (config:any) {
          jspreadsheet(document.getElementById("source_spreadsheet")!, config);
        },
      });
    });


   
  return (
    <div>
      <div className="row">
            <div className="col-md-12">
              <div
                id="source_spreadsheet"
                className="table-responsive"
              ></div>
            </div>
            <div className="col">
            </div>
          </div>
    </div>
  );
}
