import React, {
  useState,
  useEffect,
  ReactComponentElement,
  ReactNode,
  useCallback,
  ReactElement,
} from "react";
import { Steps, Panel, Placeholder, ButtonGroup, Button } from "rsuite";
import UploadForm from "./UploadForm";
import SpreadSheet from "./SpreadSheet";
import ForecastSheet from "./ForecastSheet";
import FileSaver from "file-saver";
import createName from "../pages/api/utils/name_util";
import read_spreadsheet from "../pages/api/utils/read_spreadsheet";
import Spinner from "./Spinner";
import Description from "./Description";
import axios, { AxiosProgressEvent } from "axios";
import ChartContainer from "./Chart";


const Stepper = () => {
  const [step, setStep] = useState<number>(0);
  const [sheet, setSheet] = useState<any>(null);
  const [forecast, setForecast] = useState<Array<number>>();
  const [stepContent, setStepContent] = useState<ReactElement>();
  const [icon, setIcon] = useState<string>();
  const [error, setError] = useState();
  const [chartData, setChartData] = useState();
  const [forecastBuffer, setForecastBuffer] = useState();
  const getForecast = () => {
    const body = new FormData();
    body.append("file", sheet);

    axios
      .post(`/api/foreCast`, body, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
        responseType: "arraybuffer",
        onUploadProgress: (progressEvent: AxiosProgressEvent) => {
          setStepContent(<Spinner></Spinner>);
        },
      })
      .then((response) => {
        console.log({ APIresInuseEffect: response.data });
        const data_buffer = Buffer.from(response.data, "utf-8");
        setForecast([...data_buffer]);
        setForecastBuffer(data_buffer);
      })
      .catch((err) => {
        setError(err);
        console.log(err);
      });
  };

  useEffect(() => {
    console.log(forecast);
    step == 1
      ? setIcon("fa fa-chevron-right")
      : step == 2
      ? setIcon("fa fa-line-chart")
      : step == 3
      ? setIcon("fa fa-download")
      : null;

    CreateStepContent(step);

    if (!forecast) return;
    setChartData(read_spreadsheet(forecast));
  }, [step, forecast]);

  useEffect(() => {}, []);

  const onChange = (nextStep: number) => {
    setStep(nextStep < 0 ? 0 : nextStep > 3 ? 3 : nextStep);
    console.log(step);
  };

  const onNext = () => {
    onChange(step + 1);
    if (sheet && step == 2) {
      getForecast();
      setIcon("fa fa-line-chart");
    } else if (forecast && step == 3) {
      downloadSpreadsheet();
    }
  };

  const onPrevious = () => {
    onChange(step - 1);
    step == 1
      ? (setSheet(null), setForecast(null), setChartData(null))
      : step == 2
      ? (setChartData(null), setForecast(null))
      : null;
  };

  const uploadSheet = async (sheet: any) => {
    setSheet(sheet);
    console.log(sheet);
  };

  const downloadSpreadsheet = () => {
    if (forecastBuffer) {
      const { buffer } = forecastBuffer;
      const blob = new Blob([buffer]);
      // const blob= new Blob(forecast, {type: "octet/stream"});
      const file = new File([blob], `${createName()}.xlsx`, {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8",
      });
      FileSaver.saveAs(file);
    }
  };

  const CreateStepContent: ReactElement = (step: number) => {
    let content: ReactElement;
    switch (step) {
      case 0:
        content = <Description></Description>;
        break;
      case 1:
        content = <UploadForm uploadSheet={uploadSheet}></UploadForm>;
        break;
      case 2:
        content = (
          <>
            <SpreadSheet sheet={sheet}></SpreadSheet>
          </>
        );
        break;
      case 3:
        content = (
          <>
            {forecast && (
              <>
                <ForecastSheet sheet={forecast}></ForecastSheet>

              </>
            )}
          </>
        );
        break;
    }
    setStepContent(content!);
  };

  return (
    <div className="row">
      <div className="col-lg-12">
        <Steps small={true} current={step}>
          <Steps.Item title="Start" />
          <Steps.Item title="Upload" />
          <Steps.Item title="Forecast" />
          <Steps.Item title="Download" />
        </Steps>
        <hr />
        <ButtonGroup className="row justify-content-between">
          <Button
            className="btn btn-primary"
            onClick={onPrevious}
            disabled={step === 0}
          >
            Previous
          </Button>

          <Button
            className="btn btn-primary"
            onClick={onNext}
            disabled={step === 4 || (step == 1 && !sheet)}
          >
            {sheet && step == 2
              ? "Forecast"
              : forecast && step == 3
              ? "Download"
              : "Next"}{" "}
            <i className={icon ? icon : "fa fa-chevron-right"}></i>
          </Button>
        </ButtonGroup>
        <Panel>
       <div className={chartData? 'row':''}>
            {!error && stepContent}
            {step === 3 && forecast && chartData && (
              <ChartContainer chartData={chartData}></ChartContainer>
            )}

            {error && error}
            
        </div>
        </Panel>
        <hr />
      </div>
     
    </div>
  );
};

export default Stepper;
