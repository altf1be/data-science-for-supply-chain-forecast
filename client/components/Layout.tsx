import NavLeft from './NavLeft'
import NavTop from './NavTop'
import Link from "next/link";
import type { ReactElement, ReactNode } from 'react'
import { FC } from 'react'

type MycomponenetProps= {
    children: ReactNode
}
export default function Layout({ children }: MycomponenetProps) {
  return (
    <>
      <div id="wrapper">
        <NavLeft />
        <div id="page-wrapper" className="gray-bg">
          {/* <div className="row border-bottom">
            <NavTop />
          </div> */}
          {/* wrapper wrapper-content */}
          <div className="animated fadeInRight mb-5">
            <div className="row">
              <div className="col-lg-12">
                <div className="m-t-lg">
                  {children}
                </div>
              </div>
            </div>
          </div>
          <div className="footer">
            <div>
              <strong>Copyright</strong> Supply Chain &copy; 2022-
              {new Date().getFullYear()} |{" "}
              <Link href="/legal/privacy_policy" target="_blank">
                Privacy Policy
              </Link>
              |{" "}
              <Link href="/legal/terms_and_conditions" target="_blank">
                Terms &amp; Conditions
              </Link>{" "}
            </div>
          </div>
        </div>
      </div>
    </>
  )
}