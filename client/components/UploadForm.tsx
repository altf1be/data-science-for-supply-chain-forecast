import { useState, useEffect } from "react";
import { isConstructorDeclaration } from "typescript";
export default function UploadForm(props) {
  const [file, setFile] = useState(null);
  const [isValid, setValid] = useState<Boolean>(false);

  const validate = (filename: any) => {
    const valid_extensions = ["xlsx"];

    let valid = filename.split(".")[1].includes(valid_extensions)
      ? true
      : false;
    console.log(valid);
    setValid(valid);
  };
  const uploadToClient = (event: any) => {
    console.log(event.target.files[0]);
    if (event.target.files && event.target.files[0]) {
      const i = event.target.files[0];

      validate(i.name);
      setFile(i);

      if (isValid) {
        console.log("is valid");
        props.uploadSheet(i);
      }
    }
  };

  useEffect(() => {
    console.log(file);
    console.log(isValid);
    props.uploadSheet(file);
  }, [isValid]);
  return (
    <>
      <div className="row">
        <div className="col-md-6 offset-md-3">
          <div className="ibox">
            <div className="ibox-content">
              <form onChange={uploadToClient} action="#" id="dropzoneForm">
                <div className="dz-default dz-message">
                  <input name="file" type="file" multiple />
                </div>
              </form>
              {file && !isValid && (
                <div className="alert alert-danger">
                  Only xlsx file extensions are accepted!.{" "}
                  <a className="alert-link" href="#">
                    Alert Link
                  </a>
                  .
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
