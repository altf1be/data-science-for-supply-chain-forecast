import Image from "next/image";
import { useEffect } from "react";
import spreadsheet_img from "../public/Screenshot.jpg";

const text_style = {
  fontSize: "12px",
  textTransform: "uppercase",
  fontFamily: "'Gambetta', serif",
  letterSpacing: "",
  transition: "700ms ease",
  fontVariationSettings: "wght 311",
  marginBottom: "0.8rem",
  color: "white",
  //   backgroundColor:"#145369 !important",
  outline: "none",
  textAlign: "center",
};
export default function Description() {
  useEffect(() => {
    console.log(process.env.NEXT_PUBLIC_TEMPLATE_URL);
  });
  return (
    <>
      <div className="row">
        <div className="col-md-6 offset-md-3">
          <div className="faq-item row">
            <div className="col-lg-12">
              <div className="row  justify-content-center">
                <h4 className="mr-2">{"How it Works"} </h4>
                <a data-toggle="collapse" href="#faq1" className="faq-question">
                  <i className="fa fa-info-circle" aria-hidden="true"></i>{" "}
                  <i className="fa fa-chevron-down" aria-hidden="true"></i>
                </a>
              </div>
            </div>

            <div className="col-lg-12">
              <div className="ibox">
                <div id="faq1" className="panel-collapse collapse ">
                  <div className="faq-answer border-white bg-white">
                    <div className="row">
                      <div className="col-lg-12  p-sm">
                        <div className="row justify-content-center">
                          <p>Here is the expected demand plan</p>
                        </div>
                      </div>
                      <div className="col-lg-12">
                        <Image
                          width={400}
                          className="img-fluid img-shadow rounded mx-auto d-block"
                          src={spreadsheet_img}
                          alt="Expected Demand plan"
                        ></Image>
                        <div className="row justify-content-center">
                          <button
                            onClick={() => {
                              const link = document.createElement("a");
                              link.href = process.env.NEXT_PUBLIC_TEMPLATE_URL;
                              link.click();
                            }}
                            type="button"
                            className="btn btn-primary btn-rounded mt-2"
                          >
                            &nbsp;{" Download "}&nbsp;
                            <i
                              className="fa fa-download"
                              aria-hidden="true"
                            ></i>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
