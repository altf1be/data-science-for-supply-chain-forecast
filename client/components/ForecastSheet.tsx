import { useState, useEffect } from "react";
import jspreadsheet from "jspreadsheet";
import "/node_modules/jspreadsheet/dist/jspreadsheet.css";
import "/node_modules/jsuites/dist/jsuites.css";
import "jspreadsheet/dist/jspreadsheet.css";
import "jsuites/dist/jsuites.css";
// import importer from "../node_modules/@jspreadsheet/importer/dist/index";
import parser from "@jspreadsheet/parser";
import formula from "@jspreadsheet/formula-pro";

jspreadsheet.setLicense(
  "ZjI5NWE5YmI0YzZiMjZmODRkMTg2Y2RlZDNlZjQzYWUxMTk0N2Q3NzRkYmRhZTBhOTQyNGQ3YmUwZjVlZjUzZTQzZjJmNzE3MjliNTZiMzQwZjU0YTczMWI4ZjQ3MWYzZmI3MDc5YTBmZTc3NzhjY2I0ZmZiOWVmZWYwMDRlMzksZXlKdVlXMWxJam9pUkVKZlpYaHdaWEp0YVc1MGFXNW5JaXdpWkdGMFpTSTZNVFkzTVRBMk1qUXdNQ3dpWkc5dFlXbHVJanBiSWlJc0lteHZZMkZzYUc5emRDSmRMQ0p3YkdGdUlqb3dMQ0p6WTI5d1pTSTZXeUoyTnlJc0luWTRJaXdpZGpraVhYMD0="
);
jspreadsheet.setExtensions({ parser });

export default function SpreadSheet({ sheet }) {
  jspreadsheet.setLicense(
    "ZjI5NWE5YmI0YzZiMjZmODRkMTg2Y2RlZDNlZjQzYWUxMTk0N2Q3NzRkYmRhZTBhOTQyNGQ3YmUwZjVlZjUzZTQzZjJmNzE3MjliNTZiMzQwZjU0YTczMWI4ZjQ3MWYzZmI3MDc5YTBmZTc3NzhjY2I0ZmZiOWVmZWYwMDRlMzksZXlKdVlXMWxJam9pUkVKZlpYaHdaWEp0YVc1MGFXNW5JaXdpWkdGMFpTSTZNVFkzTVRBMk1qUXdNQ3dpWkc5dFlXbHVJanBiSWlJc0lteHZZMkZzYUc5emRDSmRMQ0p3YkdGdUlqb3dMQ0p6WTI5d1pTSTZXeUoyTnlJc0luWTRJaXdpZGpraVhYMD0="
  );
  jspreadsheet.setExtensions({ parser });

  useEffect(() => {
    jspreadsheet.setExtensions({ formula, parser });
    // Destroy any existing JSS
    jspreadsheet.destroy(document.getElementById("source_spreadsheet"));

    jspreadsheet.parser({
      file: sheet,
      onload: function (config) {
        const spreadsheet_length = config.worksheets.length;
        config.worksheets.pop();
        config.worksheets.map((worksheet) => {
          worksheet.resize = "both";
          // worksheet.tableOverflow= true
          worksheet.textOverflow = false;
          worksheet.minDimensions = [
            worksheet.minDimensions[0],
            worksheet.minDimensions[1],
          ];
        });

        jspreadsheet(document.getElementById("source_spreadsheet"), config);
      },
    });
  });

  return (
    <div className="col-lg-6">
      <div className="ibox">
        <div className="ibox-title"> 
        <h5> Moving Average Forecast - Spreadsheet view</h5>
       
        </div>
          <div className="ibox-content">
            <div id="source_spreadsheet" className="table-responsive"></div>
          </div>
       
      </div>
    </div>
  );
}
