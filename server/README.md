# data-science-for-supply-chain-forecast

Write code presented in the book "data science for supply chain forecast" written by Nicolas Vandeput <https://supchains.com/>

# setup
* install python3 preferably 3.8
* go to directory flaskr`cd flaskr`
* create a python virtual evnironement `python3 -m venv venv`
* 
* run `. venv/bin/activate`
    * run  `pip install -r requirements.txt`
* run ``
* run 
    * `export FLASK_APP=main.py`
    * `export FLASK_ENV=development`
   (or add them to .env file) 
    * `flask run`

# expose the LOCAL backend with ngrok:
* `ngrok http 5000`
* set the `API_URL` environment variable in vercel to the ngrok link <https://vercel.com/alt-f1/data-science-for-supply-chain-forecast/settings/environment-variables>
* redeploy the app on vercel
* open the page <https://data-science-for-supply-chain-forecast.vercel.app>
* run the server using the script in file `server/run_app_prod.bat`
# Tools

* pandas <https://pandas.pydata.org>
    * Pandas Dataframe: plot examples with Mathplotlib and Pyplot : <http://queirozf.com/entries/pandas-dataframe-plot-examples-with-matplotlib-pyplot>
    * Summarising, Aggregating, and Grouping data in Python Pandas <https://www.shanelynn.ie/summarising-aggregation-and-grouping-data-in-python-pandas>
    * Flatten Nested JSON in Pandas <https://www.kaggle.com/jboysen/quick-tutorial-flatten-nested-json-in-pandas>
* NumPy <https://numpy.org>
* Plot.ly <https://plot.ly>
    * Python Plotting for Exploratory Data Analysis <https://pythonplot.com>

* Python 3.8+ <https://www.python.org/downloads>
    * Understanding slice notation <https://stackoverflow.com/questions/509211/understanding-slice-notation>
* visual studio code <https://code.visualstudio.com>

* ngrok ngrok http --host-header=rewrite 5000
* ngrok <https://ngrok.com>

# License

(c) alt-f1 sprl <http://www.alt-f1.be, Abdelkrim Boujraf <http://www.alt-f1.be/literature.html>

This work is licensed under a Creative Commons Attribution 4.0 International License : <http://creativecommons.org/licenses/by/4.0>

All trademarks mentioned belong to their owners, third party brands, product names, trade names, corporate names and company names mentioned may be trademarks of their respective owners or registered trademarks of other companies and are used for purposes of explanation and to the owner's benefit, without implying a violation of copyright law.