data = {
    "title":"Time-Price Comparison",
    "subtitle":"Scatter plot of time vs price.",
    "data":[
        {"time":2,"price":1,"callout":False},
        {"time":3,"price":2,"callout":False},
        {"time":4,"price":3,"callout":True},
        {"time":5,"price":4,"callout":True},
        {"time":6,"price":5,"callout":False}
    ],
    "xlabel":"Time (PM)",
    "ylabel":"Price",
    "caption":"Note: Data made up from my imagination and therefore not real. [2020]"
}
layout = {
    "data": {
        "time_min":1,
        "time_max":7,
        "price_min":0,
        "price_max":6
    },
    "plot": {
        "xmin":  80,
        "ymin": 110,
        "xmax": 565,
        "ymax": 300,
        "pad":  10,
        "point_radius": 5
    },
    "color": {
        "color_on": "hsl(230,70%,60%)",
        "color_off": "hsl(0,0%,50%)",
        "axis": "hsl(0,0%,30%)",
        "title": "hsl(0,0%,0%)",
        "subtitle": "hsl(0,0%,50%)",
        "caption": "hsl(0,0%,50%)",
        "background": "hsl(0,0%,95%)"
    }
}