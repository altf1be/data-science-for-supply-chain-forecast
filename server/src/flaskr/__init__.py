""" main entry module: register all blueprints"""
from flask import Flask
from flask_cors import CORS
from .forecast_blueprints.forecast_endpoints import bp as charts


def create_app():
    """
    initiate flask app
    """
    app = Flask(__name__)
    CORS(app)
    print(f'Flask.instance_pathath:{app.instance_path}')
    app.register_blueprint(charts)
    return app
