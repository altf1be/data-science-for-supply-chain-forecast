import base64
from io import BytesIO
from matplotlib.figure import Figure
from flaskr.utils.getMovingAverage import (moving_average)

class chart_factory:
    
    def __init__(self, dt):
        self.data= dt


    def console_log(self):
        print(self.data)

    def png_chart_farcory(self):
         
         df = moving_average(self.data)
         fig = Figure()
         ax = fig.subplots()
         ax.plot(df)
         # Save it to a temporary buffer.
         buf = BytesIO()
         fig.savefig(buf, format="png")
         # Embed the result in the html output.
         data = base64.b64encode(buf.getbuffer()).decode("ascii")
         return data
