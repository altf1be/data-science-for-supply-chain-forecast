"""module that manage the naming and creation of newly created directories"""
from datetime import datetime
import os
import errno
import uuid
from dotenv import load_dotenv
load_dotenv()


class DirectoryManager:
    """class that provide methods to manage naming and creation of new direrctories and files"""

    def __init__(self):
        self.base_path = os.path.abspath(os.getcwd())
        self.spreadsheet_upload_directory = os.getenv(
            "spreadsheet_upload_directory")

    def generate_directory_prefix(self):
        """method that generate date based names for directories"""
        now_text = datetime.strftime(
            datetime.now(), "%Y-%m-%d_%Hh%Mm%Ss"
        )
        # directory_prefix = f'{now_text}-{str(uuid.uuid4())}'
        # sample of now_text: 2023-01-02_18h07m33s
        directory_prefix =  f'{now_text}-{str(uuid.uuid4())}'
        # print(directory_prefix)
        return directory_prefix

    def create_directory(self):
        """method that create directories"""
        print("create_directory")
        # pylint: disable=line-too-long:
        # path = f'{self.base_bath}{self.spreadsheet_upload_directory}/{self.generate_directory_prefix()}'
        path = os.path.join(self.base_path,self.spreadsheet_upload_directory,self.generate_directory_prefix())
        try:
            os.makedirs(path)
            return path
        except OSError as error:
            print(error.errno)
            if error.errno != errno.EEXIST:
                raise
