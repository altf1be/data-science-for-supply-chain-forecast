"""Module that generate key performance indicators"""
import numpy as np
import pandas as pd


def key_performance_indicator_to_dataframe(dataframe):
    """Method that return a dataframe with kpis: bais, MAPE, MAE and RMSE"""
    try:
        dataframe_transposed= dataframe.T
        print(f'transposed moving average dataframe: {dataframe_transposed}')
        dem_ave = dataframe_transposed.loc[dataframe_transposed["Error"].notnull(), "Demand"].mean()
        bias_abs = dataframe_transposed['Error'].mean()
        bias_rel = bias_abs / dem_ave
        mean_absolute_percentage_error = (
            dataframe_transposed["Error"].abs()/dataframe_transposed["Demand"]).mean()
        mean_absolute_error_abs = dataframe_transposed["Error"].abs().mean()
        mean_absolute_error_rel = mean_absolute_error_abs / dem_ave
        root_mean_square_error_abs = np.sqrt((dataframe_transposed['Error']**2).mean())
        root_mean_square_error_rel = root_mean_square_error_abs / dem_ave

        kpi_dictionary = {
            "Bias": [bias_rel, bias_abs],
            "mean_absolute_percentage": mean_absolute_percentage_error,
            "mean_absolute_error": [mean_absolute_error_abs, mean_absolute_error_rel],
            "root_mean_square_error_rel": [root_mean_square_error_abs, root_mean_square_error_rel]
        }

        kpi_df = pd.DataFrame(data=kpi_dictionary)
        kpi_df_transposed = kpi_df.set_index('Bias').T
        kpi_df_transposed.columns = ["Absolute", "Scaled"]
        print(f'kpi df transposed: {kpi_df_transposed}')
        kpi_complete_df = pd.concat(
            [dataframe_transposed, kpi_df_transposed], axis=0, ignore_index=True)
        print(f'concatenated kpi and moving average dfs: {kpi_complete_df}')
    except Exception as err:
        print(f"Unexpected {err=}, {type(err)=}")
        raise

    # return {"Bias": {bias_rel, bias_abs},
    #         "MAPE": MAPE,
    #         "MAE": {mean_absolute_error_abs, mean_absolute_error_rel},
    #         "root_mean_square_error_rel": {root_mean_square_error_abs, root_mean_square_error_rel}
    #         }
    return kpi_complete_df
