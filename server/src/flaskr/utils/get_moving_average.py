"""Module that generates demand moving average"""
import numpy as np
import pandas as pd


def get_moving_average(demand, date, extra_periods=4, number_of_periods=3):
    """Method that return the moving average as a dataframe"""
    demand = np.array(demand)
    cols = len(demand)
    demand = np.append(demand, [np.nan] * extra_periods)
    date= np.append(date, [np.nan] * extra_periods)
    forecast = np.full(cols + extra_periods, np.nan)

    for time in range(number_of_periods, cols + 1):
        forecast[time] = np.mean(demand[time - number_of_periods: time])

    # f[cols + 1 :] = f[time ]
    forecast[time + 1:] = np.mean(demand[time - number_of_periods:time])
    dataframe = pd.DataFrame.from_dict(
        {"Date": date,
         "Demand": demand,
         "Forecast": forecast,
         "Error": demand - forecast},
         orient='index')
    print(f'moving average forecast: {dataframe}')
    return dataframe
