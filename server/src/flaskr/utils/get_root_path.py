"""Module to get root prject path from sub directories"""
from pathlib import Path

def get_project_root() -> Path:
    """Method that return the project root path from any nested and deep nested directories"""
    return Path(__file__).parent.parent
    