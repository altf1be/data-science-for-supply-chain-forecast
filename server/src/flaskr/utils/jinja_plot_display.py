import jinja2

# Use the string to create a jinja template
jinja_template = jinja2.Template(
    plot_template
)

# Render it by passing the data
# to the template
svg_string = jinja_template.render(
    data=data,
    layout=layout,
    xticks=xticks,
    yticks=yticks,
    xscale=xscale,
    yscale=yscale
)

# The result is just a string
# with the rendered plot
type(svg_string) == str


# Now, optionally, if you want
# to display the SVG in Jupyter
# you can do this...

# from IPython.display import display_svg

# display_svg(
#   svg_string,
#   raw=True
# )