def MAPE(df):
   dem_ave= df.loc[df["Error"].notnull(),"Demand"].mean()
   bias_abs= df['Error'].mean()
   bias_rel= bias_abs / dem_ave
   MAPE = (df["Error"].abs()/df["Demand"]).mean()
   try:
     print(bias_abs,bias_rel)
     print('Bias: {:0.2f}, {: .2%}'.format(bias_abs,bias_rel), 'MAPE', MAPE)
   except Exception as err:
        print(f"Unexpected {err=}, {type(err)=}")
        raise
   
   return MAPE