"""forecast api endpoints """
import os
from flask import Blueprint, request, flash, redirect, send_file
from werkzeug.utils import secure_filename
from flaskr.utils.get_moving_average import (
    get_moving_average
)

from .spreadsheet_processor import (SpreadsheetProcessor)


bp = Blueprint('charts', __name__, url_prefix='/forecast')

ALLOWED_EXTENSIONS = {'xlsx'}


def allowed_file(filename):
    """function that validate the received file extension if it belongs to accepted extentions"""
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@bp.route('movingaverage', methods=['POST'])
def get_forecast():
    """route bound function that receive a xlsx file and apply forcast method against it"""
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            spreadsheet_processor = SpreadsheetProcessor(
                filename=None, file_blob=None)
            response = spreadsheet_processor.get_forecast(
                filename=filename, file_blob=file)
            return send_file(path_or_file= response, download_name =os.path.basename(response))
            # return response
            #
            # send_file(
            #     spreadsheet,
            #     mimetype='text/csv',
            #     download_name= f'{filename.rsplit(".",1)[0]}.csv',
            #     as_attachment=True

            # )
            # file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            # return redirect(url_for('download_file', name=filename))
