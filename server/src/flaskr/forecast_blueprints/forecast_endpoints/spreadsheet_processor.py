"""process the spreadsheet: store, forcast demand, generate csv, generate xlsx, produce charts"""
from pathlib import Path
import os
import io
import random
import pandas as pd
import numpy as np
import xlsxwriter

from flask import make_response
from flaskr.utils.get_moving_average import (get_moving_average)
from flaskr.utils.key_performance_indicator_to_dataframe import (
    key_performance_indicator_to_dataframe)
from flaskr.utils.directory_manager import (DirectoryManager)


class SpreadsheetProcessor:
    """A Class that manages processing the spreadsheet forecast"""
    dataframe = pd.DataFrame()

    def __init__(self, filename, file_blob):
        self.filename = filename
        self.file_blob = file_blob
        self.directory_management = DirectoryManager()

    def store_spreadsheet(self, file_blob, filename):
        """
        A dummy docstring.
        """
        print('store_spreadsheet')
        try:
            path = self.directory_management.create_directory()
            print(f'store file in: {path}')
            # print(type(file_blob))
            #  self.SPREADSHEET_BLOB.to_excel(f'{path}/{self.SPREADSHEET_FILE_NAME}')
            print(f'file_path_name: {os.path.join(path, filename)}')
            file_blob.save(os.path.join(path, filename))
            return f'{path}'
        except OSError as error:
            print(f'error: {error}')
            print("File descriptor is not associated with any terminal device")

    def create_chart(self, input_dataframe, path, filename, dataframe):
        """Method that create new output sheet and chart"""
        letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G',
                        'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P']
        # _path = path.rsplit(filename, 1)[0]

        # workbook = xlsxwriter.Workbook(f'{_path}/chart.xlsx')
        moving_average_filename= f'{os.path.splitext(filename)[0]}-moving_average.xlsx'
        workbook = xlsxwriter.Workbook(os.path.join(path,moving_average_filename), {
                                       'nan_inf_to_errors': True})
        # try:
        #     writer = pd.ExcelWriter(f'{_path}/chart.xlsx', engine='xlsxwriter')# pylint: disable=abstract-class-instantiated
        #     input_dataframe.to_excel(excel_writer=writer,
        #                index=False, sheet_name='input_sheet')

        # except Exception as e:
        #     print("Close the file: ",e)
        worksheet_00 = workbook.add_worksheet('source_data')

        worksheet_01 = workbook.add_worksheet('moving_average_data')
        worksheet_02 = workbook.add_worksheet('moving_average_chart')
        # chartsheet = workbook.add_chartsheet()
        bold = workbook.add_format({'bold': 1})
        headings = [*input_dataframe.columns]
        (max_row, max_col) = input_dataframe.shape

        data_from_input = []
        for col in input_dataframe:
            input_dataframe.fillna('', inplace=True)
            array_to_append = np.array(
                # input_dataframe[col].loc[input_dataframe[col].notnull()]
                input_dataframe[col]
            )

            data_from_input.append(array_to_append)

        print("dataArr:", data_from_input)

        for idx, _ in enumerate(data_from_input):

            worksheet_00.write_row('A1', headings, bold)
            worksheet_00.write_column(f'{letters[idx]}2', data_from_input[idx])

        # worksheet_00.write_column('A2', data_from_input[0])
        # worksheet_00.write_column('B2', data_from_input[1])
        # worksheet_00.write_column('C2', data_from_input[2])

        bold = workbook.add_format({'bold': 1})
        headings = [*dataframe.columns]
        (max_row, max_col) = dataframe.shape
        print(max_col)

        data = []
        for col in dataframe:
            dataframe.fillna('', inplace=True)
            array_to_append = np.array(
                # dataframe[col].loc[dataframe[col].notnull()]
                dataframe[col]
            )

            data.append(array_to_append)

        print(f'moving average dataArr: {data}')
        for idx, _ in enumerate(data):
            if idx < len(data):
                worksheet_01.write_row('A1', headings, bold)
                worksheet_01.write_column(f'{letters[idx]}2', data[idx])

        # worksheet_01.write_row('A1', headings, bold)
        # worksheet_01.write_column('A2', data[0])
        # worksheet_01.write_column('B2', data[1])
        # worksheet_01.write_column('C2', data[2])
        # worksheet_01.write_column('D2', data[3])
        chart1 = workbook.add_chart({'type': 'line'})
        print(
            f'df headings: {headings[1]}:{dataframe[headings[1]].loc[dataframe[headings[1]].notnull()]}')

        for index, _ in enumerate(headings):

            col = index
            if index > 0:
                print("column_name: ", col)
                chart1.add_series({
                    'name':       ['moving_average_data', 0, col],
                    'categories': ['moving_average_data', 1, 0,   max_row, 0],
                    'values':     ['moving_average_data', 1, col, max_row, col],
                    'line': {
                        # pylint: disable=line-too-long
                        'color': "#FF2D00" if headings[index] == "Error" else "#1ab394" if headings[index] == "Demand" else "#CBCBCB" if headings[index] == "Forecast" else f'{["#"+"".join([random.choice("ABCDEF0123456789") for i in range(6)])][0]}'
                    }
                })

        chart1.set_title({'name': 'Moving Average'})
        chart1.set_x_axis({'name': 'Periods'})
        chart1.set_y_axis({'name': 'Volume'})
        chart1.set_style(11)
        worksheet_02.insert_chart('D9', chart1)
        # chartsheet.activate()
        # workbook.save()
        workbook.close()
        return os.path.join(path,moving_average_filename)
    

    def spreadsheet_to_dataframe(self, path, filename):
        """Method that craete a dataframe from the spreadsheet and return the dataframe"""
        # print(path)
        self.__class__.dataframe = pd.read_excel(Path(path + "/" + filename))
        return self.__class__.dataframe

    def get_demand(self):
        """method that return all the demand column values"""
        demand = self.dataframe["Demand"]
        return demand

    def get_date(self):
        """Method that return the date column from the dataframe"""
        date = self.__class__.dataframe["Date"]
        return date

    def get_demand_movingaverage(self, demand):
        """GET DEMAND MOVING AVERAGE as a dataframe"""
        demand = self.get_demand()
        date = self.get_date()
        demand_average = get_moving_average(demand=demand, date=date)
        return demand_average

    def get_kpi(self, moving_average):
        """GET KPI """
        result = key_performance_indicator_to_dataframe(moving_average)
        return result

    def dataframe_to_spreadsheet(self, demand_average):
        """method that convert a dataframe to a spreadsheet"""
        try:
            out = io.BytesIO()
            with pd.ExcelWriter(out, engine='xlsxwriter') as writer:  # pylint: disable=abstract-class-instantiated
                demand_average.to_excel(excel_writer=writer,
                                        index=False, sheet_name='chapter_01')
            # writer.save()
                writer.close()
            response = make_response(out.getvalue())
            response.headers["Content-Disposition"] = "attachment; filename=export.xlsx"
            # pylint: disable=line-too-long:
            response.headers["Content-type"] = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            response.headers['Access-Control-Allow-Origin'] = "*"
            return response
        except Exception as err:
            print(f"Unexpected {err=}, {type(err)=}")
            raise

    def dataframe_to_csv(self, demand_average, path, file):
        """Save the dataframe to csv file"""
        try:
            filename = file.rsplit('.', 1)[0]
            csv_path=os.path.join(path,f'{filename}.csv')
            demand_average.to_csv(csv_path)
            print(f'csv path: {csv_path}')
            return csv_path
        except OSError as error:
            print(f'dataframe_to_csv error: {error.errno}')
            raise

    def get_forecast(self, filename, file_blob):
        """Method that return the generated spreadsheet path"""
        generate_dataframe_file_path = self.store_spreadsheet(
            file_blob, filename)
        dataframe = self.spreadsheet_to_dataframe(
            path=generate_dataframe_file_path, filename=filename)
        print(f'uploaded source: {dataframe}')
        # self.create_chart(path=generate_dataframe_file_path,filename=filename,dataframe=dataframe)
        demand = self.get_demand()
        moving_average = self.get_demand_movingaverage(demand=demand)
        kpi_result = self.get_kpi(moving_average=moving_average)
        self.dataframe_to_csv(
            demand_average=kpi_result, path=generate_dataframe_file_path, file=filename)
        workbook_path = self.create_chart(
            input_dataframe=self.__class__.dataframe,
            path=generate_dataframe_file_path,
            filename=filename, dataframe=moving_average.T
        )
        print(workbook_path)
        # self.dataframe_to_csv(moving_average, generate_dataframe_file_path, file=filename)
        # workbook_path=self.create_chart(path=generate_dataframe_file_path,
        #                   filename=filename, dataframe=moving_average)
        # print(workbook_path)
        return workbook_path
        # return self.dataframe_to_spreadsheet(moving_average)

    # def getPath(self):
    #     absolute_path=os.path.abspath(os.getcwd())
    #     self.directory_management.create_directory()
    #     return absolute_path
