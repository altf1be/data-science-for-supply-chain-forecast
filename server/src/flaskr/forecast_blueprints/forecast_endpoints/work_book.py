"""module that produces charts"""
import xlsxwriter


class Workbook:
    """class that provide methods to produce and manapulate charts"""

    def __init__(self, workbook_name):
        self.workbook_name = workbook_name

        self.workbook = xlsxwriter.Workbook(str(self.workbook_name) + '.xlsx')

        self.letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G',
                        'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P']

    def produce(self):
        """method tht clsoe the workbook creation"""
        # self.workbook.close()
        print('Created ' + str(self.workbook_name) + '.xlsx')

    def print_frame(self, worksheet, dataframe, df_width, start_data_index):
        """method that produce a sheet"""
        col_as_lists = []
        col_names = list(dataframe.columns.values)

        # loops through columns in df and converts to list
        for number in range(0, df_width):
            col_n = dataframe[col_names[number]].tolist()

            # checks to see if column has numbers, if so -> convert to float!
            if number < start_data_index:
                col_n.insert(0, col_names[number])

            elif self.is_number(col_n[0]):
                convert = col_n[0:]
                convert = [float(x) for x in convert]
                convert.insert(0, col_names[number])
                col_n = convert
            else:
                col_n.insert(0, col_names[number])

            col_as_lists.append(col_n)

            # Prints each list into the worksheet.
            worksheet.write_column(
                self.letters[number] + '1', col_as_lists[number])

        # Formats numerical data as percentage
        percentformat = self.workbook.add_format({'num_format': '0%'})
        worksheet.set_column(
            self.letters[start_data_index] + ':' + self.letters[df_width], None, percentformat)

    def add_chart(self, dataframe, tab_name, start_data_index):
        """method that draw and add a chart to a sheet"""
        df_width = len(dataframe.columns)

        worksheet = self.workbook.add_worksheet(tab_name)
        self.print_frame(worksheet, dataframe, df_width, start_data_index)

        chart = self.workbook.add_chart({'type': 'column'})
        df_length = (len(dataframe.index))

        for number in range(start_data_index, df_width):

            chart.add_series({
                # pylint: disable=line-too-long:
                'name': '=' + tab_name + '!$' + self.letters[number] + '$1',
                'categories': '=' + tab_name + '!$' + self.letters[start_data_index - 1] + '$2:$' + self.letters[start_data_index - 1] + '$' + str(df_length + 1),
                'values': '=' + tab_name + '!$' + self.letters[number] + '$2:$' + self.letters[number] + '$' + str(df_length + 1),
                'fill': {'color': '#FFB11E'},
                'data_labels': {'value': True, 'center': True}
            })

        chart.set_title({'name': tab_name})
        chart.set_x_axis({'major_gridlines': {'visible': False}})
        chart.set_y_axis({'major_gridlines': {'visible': False}, 'max': .70})

        worksheet.insert_chart(self.letters[df_width + 2] + '2', chart)

        return

    def is_number(self, string):
        # pylint: disable=line-too-long:
        """ Function used to help with detecting and converting floats from string to number data types."""
        try:
            float(string)
            return True
        except ValueError:
            return False
